Overview
________

A target is a unit file that exists only to group together other systemd units into a somewhat predictable set of services along with their dependencies.

Targets can be roughly analogous to runlevels but are diferent in many aspects, such as being non-exclusive.



The Target Tree
_______________

Remember that when a systemD system boots up, it looks at the default.target which is a symlink to the actual configured target the system is configured to boot to.  i.e. - A desktop is configured to boot upto the graphical.target, a non-gui system is configured to boot upto multi-user.target, etc, etc.

So as soon as SystemD reads that default.target, it works it's way backward to all targets that lead upto that final default.target and builds the dependencies for each target and then starts booting.



Runlevels vs Targets
____________________

So because targets are analogous to runlevels, targets can be seen a system state that you arrive to when they're reached.

poweroff.target - This gets the system to a shutdown state.

rescue.target - This gets the systemn upto a state where only the bare minimum of services are started so a rescue can be attempted.

multi-user.target - This target gets the system upto a state where multiple users can log into the system and use it (no gui)

graphical.target - This is a system state where multiple users can also login and use the system but includes a gui/desktop

reboot.target - This shuts down the system in an orderly fashion and starts it back up



Querying And Changing System States
___________________________________

systemctl
	-list-unit -t target - this lists all the targets currently loaded on the system

	-start <target>.target - this allows you to change to a particular target

	-isolate <target>.target - this allows you to change focus to a target (this is confusing because it was explained as restricting
		 		   the system to this target)



Viewing And Changing The Default Target
_______________________________________

systemctl
	-get-default - queries the current target set to default.target
	-set-default - sets a target to be the default.target


NOTE:  There exists a target even more stripped down than rescue.target called emergency.target.  It basically is equivalent to the state of the system RIGHT before it shuts down.



Shutting Down The System
________________________

/sbin/halt - this starts the halt.target to shutdown and halt the system.  If --force is specified it won't shutdown services?

/sbin/reboot - starts the reboot.target, send a wall message to all connected users and then after shutdown of all services will reboot the system.

/sbin/poweroff - starts the poweroff.target and after the shutting down of all services, powers off the system.

/sbin/shutdown - this uitility is an uber-command that can be used by itself or as a way to reboot, halt or poweroff the system.

NOTE:  All the above commands are symlinks to systemctl

NOTE2:  --force will skip services shutdown but will kill all processes and unmount filesystems
	--force --force (--force two times on the same line) will abruptly just shut the system down.  No stopping of services, NO killing of processes first.



Examples
________

runlevel - this shows you the current runlevel you're in

systemctl status - gets you a status of the system relative to the c groups created.  i.e. user slice and system slice.

systemctl list-units -t targets - allows you to list all the targets currently loaded on the system.

systemctl get-default - lists the currently set default target (usually graphical.target if a desktop environment)

systemctl set-default <target> - changes the default target.

systemctl isolate <target> - this changes you to a desired target/runlevel.

systemd-analyze critical-chain - this will show you a hierarchically all the targets that we're executed to reach the current system state.

#I left of working on lab.  I was changing default targets, forcing basic.target to allow to be isolated and then changing back to multi-user target to confirm critical chain.
