SystemD
_______

What is a linux system service?
     A service is a process or group of processes that run in the background and exist to provide a function or
     return data.  If they don't run in the background then they can be started on demand when a connection request
     for that service is made.  SystemD is a type of this service.


Boot Process
____________

POST->MBR->BootLoader
	|
Kernel->initramfs->systemd
	|
SystemD Hierarchy of Dependencies (Processes that are started)


SystemD Dependencies
____________________

SystemD is configured via unit files which are text files that outline the depencies of the service you want to start.  I understand these outlines are called "Dependency Directives?"

Requires
	-If Unit A Requires Unit B then they both will be activated at the same time.
	-If unit A Requires Unit B then they both will be activated at the same time.
	-Because of this strict dependancy, it is suggested the "Requires" directive be used sparingly as it will
	 limit the robostness of your system.

Wants
	-If Unit A Wants Unit B then they will attempt to start at the same time.
	-If Unit A failed there is no impact on Unit B
	-Recommended method of hooking activation together, more robust.

Before
	-If Unit A has a Before=UnitB, then Unit B isn't started until UnitA has finished starting
	-When the units are deactivated, UnitA has to wait until UnitB is deactivated before it deactivates

After
	-If UnitA has an After=UnitB, then UnitB must finish activating before UnitA starts to activate.

These dependency directives usually work in pairs, i.e. UnitA Requires UnitB but UnitB will start only after UnitA has finished loading completely.


Reducing Dependencies
_____________________

SystemD provides the facility for letting services start even when certain dependencies for that services aren't meant.  It does this by lying or faking those dependencies for that service so it can start.  i.e. if a service requires a certain filesystem to be mounted then SystemD will fake that file system so that service to start.  This only works if that services doens't need to read data from that file system.  Below are the certain dependices that SystemD will fake:

-Filesystem availabilty
-Sockets
-Dbus


Systemd Commands
__________________

systemctl
Viewing Dependencies
-list-dependencies
-list-dependencies <unit>
-list- dependencies --before
-list-dependencies --after
--with-dependencies

controling services
- -H user@host (allows you to execute systemctl on a remote host)

Reading unit files
- cat <unit> (allows you to read unit files.  systemctl cat backup_laptop.service)	

systemd-analyze
Analyzing Units/Dependencies
systemd-analyze <options>
		-critical-chain (shows you the chain of services required to reach target or service)		
		-blame (shows you the critical chain for a service but in the order from greatest to least of
		        of the time it took each services to start before the target was reached)
		-plot & dot (plots and dots graph stuff for a target?)
		-dump (dumps all info which is a lot, related to a target or service)

