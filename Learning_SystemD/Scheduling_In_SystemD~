Timers
______

Timer - A SystemD Unit that configure when a coresponding service unit is invoked.
      	  By default a timer is a unit file that is paired up with a .service unit file
	     i.e. - dothisnow.timer and dothisnow.service


Types Of Timers
_______________

Monotonic - Based on an event.  i.e. @boot

Realtime - Time based.

Transiet - One off.  i.e. - Run this command this one time (in 45 minutes, etc, etc)

NOTE:  There exist the 'at' command that let you do this.  I didn't know this!  lol


Elements Of A Timer
___________________

Timer -> Service -> [a script]

The Timer exists soley to determine the time you want something to happen

The Service can define what it is that you want to happen

The script can be what happens or you can have that defined in the service.  i.e. ExecStart=/usr/bin/foo


Querying Timers
_______________

systemctl list-timers

The nice thing about this command is it provides columns for all this info.


Analyzing Time
______________

systemd-analyze - allows you to analye timestamps and time notations to determine when a job will run.

Time Format: [DOW] Y-M-D HH:MM:SS

systemd-analyze calendar <time format> - This is the command that will let you analyze a proposed time format and confirm it's correct and if so, tell you when you in human readable form when your proposed timer will run.
    i.e. - systemd-analyze calendar Mon 21-5-1 00:00:00
    	 The above will print out:
	 Original form: Mon                        
Normalized form: Mon *-*-* 00:00:00         
    Next elapse: Mon 2021-05-31 00:00:00 MDT
       (in UTC): Mon 2021-05-31 06:00:00 UTC
       From now: 2 days left                

  Original form: 21-5-1             
Normalized form: 2021-05-01 00:00:00
    Next elapse: never              

  Original form: 00:00:00                    
Normalized form: *-*-* 00:00:00             
    Next elapse: Sat 2021-05-29 00:00:00 MDT
       (in UTC): Sat 2021-05-29 06:00:00 UTC 
       From now: 16h left

You can do things like ranges for DOW:

systemd-analyze calendar "Mon, Wed, Fri *-*-* 00:00:00"
    This amounts to every Mon, Wed and Fri @ 12AM

systemd-analyze calendar "*-*-* *:20/15:00"
    This means every hour at the 20 minutes mark and then every 15 minutes there after until the top of the hour.  Then
    it will resume to start at 20 minutes into the hour.
    
systemd-analyze calendar "*-*-* *:20/15:00" --iterations=5
    The 'iterations' option lets you see how many iterations timestamps you see in the future


systemd-analyze calendar weekly | hourly | daily | monthly
    This above obviously lets you schedule things using the notation above.

NOTES:  systemd.timers and system.time man pages are for reference.


Using Timers Effectively
________________________
		
Using Monotonic Timers
These type of timers run FROM a certain point in time , either defined specifically or based on an event such as the On* parameters below.  Default is in seconds or you can specificy "xH xxMin"

OnActiveSec - Runs relative to the moment the timer unit itself is activated.  i.e. 10 min after unit file is activated.

OnBootSec - Defines a timer relative to when the machine was booted up and can act identical to OnStartupSec.  i.e. 10 min after bootup

OnStartupSec - Defines a timer relative to when the service manager was first started (particularly useful for User Service Manager timers, which happen after the first login)

[Unit]
Description=Run foobar 10 min after boot and every week thereafter

[Timer]
OnBootSec=10min		#This means start unit 10 min after boot
OnUnitActiveSec=1w	#After this unit has been active for exactly 1 week, start unit again
Unit=foobar.service

[Install]
WantedBy=timers.target	#Pulls in timers at boot-up.

NOTE:  'RemainAfterExit' directive causes a service to stay 'active' even after it has finished running.  i.e. you want an action to take place as a result of this timer.  But if it stays in an 'active' state, then it won't run again at the specified time if it's still seen as 'active' due to the 'RemainAfterExit' directive.



Using Realtime Timers
DayOfWeek or DOW - Mon, Tues, Wed, etc etc.  (This is optional as it's known as a restriction, meaning you're restricting a timer to go off during a paticular day of the week)

Year - 2 or 4 digit year.  You can also use a range of years

Month - 1-12, range of months or individual months

Day - 1-31, range of days, individual days.

Hour - 0-24, range of hours, individual hours

Minute - 0-60, ""

Second - 0-60, ""

[Unit]
Description=Run foobar every Wed at 10:14AM

[Timer]
OnCalendar=Wed *-*-* 10:14:00
Persistent=true	     #ensures unit is executed at the moment the system realizes it was down and this
		     timer should have ran during that down time.
Unit=foobar.service

[Install]
WantedBy=timers.target


Using Transient Timers
Transiet timers are those actions that we specify on the fly to run after a specified amount of time has passed.  i.e. run command in 45 minutes

systemd-run - allows the execution of a transiet timer
    --shell|-s - option that allows the timer to be executed using the same shell environment as the
    	         current user

systemd-run --on-active=30sec /bin/touch /home/user/foobar.txt
"Running timer as unit: run-u97.timer
Will run service as unit:" run-u97.service" #output of above systemd-run command


NOTE:  If you don't use --shell|-s options, you have to specify the full path to any commands you want executed.  