#+Title: The Bottom Drawer
#+Author: Jessie Saenz a.k.a. Chunky_Pie
#+Date: <2023-03-31 Fri>
#+options: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../CSS/gray.css"

* *_The getline Function_*

So similiar to the ~next~ keyword in awk, the ~getline~ function also causes awk to go to the line of input.  However, instead of passing control back to the first line in the script, awk just continues with the next command to be operated upon.  That /new/ next line of input.

Imagin a page from your favorite novel.  Now using a ruler, underline the first line on that page (all you see is that one line right?) Think of this /one/ line as the current input line awk is operating on.  Now call the ~getline~ function.  Your ruler goes to the next line to underline.  This is now the current input line that awk will operate on, etc, etc.  This is exactly how getline works.  In my head, it works like the CRLF (carriage return, line feed) function of a type writer.  

Although ~getline~ technically is a function, it's not called like one.  You simply just call it:

#+begin_src shell
awk '/^root/{getline; print $0}' /etc/passwd
#+end_src

The above awk command matches for "root" at the beginning of a line, then operates on that line by calling the ~getline~ function which then passes the next line in the files to be current input line for operation, which is to print it.

Also like all functions, getline returns an exit status.  Below are their values and meanings:

- 1  - If getline was able to read a line
- 0  - If getline encounters the end of a file
- -1 - If getline encounters an error.  

Using the above exit status's and a while loop construct, you can force awk to operate the same command on all lines of input, before going to
the next operation.  As reviewed in previous notes, awk's natural flow is to take a single line of input and run all the operations in the script
against that /one/ line before moving to the next line of input and running all the commands over again on that next line of input.  You can do
the inverse (run the same command on all subsequent lines of input) by using the following pattern:

#+begin_src shell -n
awk '/^root/{while (getline>0) print $0}' /etc/passwd
#+end_src

The above command will continously run the ~print $0~ command on every line of input once a match for "root" at the beginning of the line is matched.  While printing $0, the ~getline~ function returns a successful exist status, which will continue until it reaches the last line of input.  Then, awk will move on to the next operation in the script.

This could be handy if you want to define a loop that reads EVERY line of input and does something with it.  Once you go through all the lines, your script will continue with the rest of your awk code:

#+begin_src shell -n
/^root/{

    while (getline>0) {

        operation

	operation

    }

    #here, awk will start at the top again and continue with the rest of your awk code below
    print $0

    another operation

}

#+end_src

** *_Reading Input_*

You can also redirect output from a file and use getline to operate on the nextline in that file.  When this happens though, all system variables like $0, $1, NF, etc, etc apply to that first line of the file (not the next one.)

#+begin_src shell -n
while ( (getline < "file") > 0)

    print
#+end_src

Another interesting things you can do with the ~getline~ function is assign the input record (the next line that getline gets you) into a variable.  Like when redirecting a file to getline, it doesn't move $0 to be that next line of input.  When you assign ~getline~ to a variable, you're still on the current input line.  So $0, is still the same, $NF, etc, etc.  The "next line" is put into that variable.  The only caveat is the record counters NR and FNR /ARE/ incremented though.

You can also run a shell command and use getline to read it's input:

#+begin_src shell -n
awk 'BEGIN{"echo what" | getline output; print output}'
what
#+end_src

If the command you're running contains multiple lines of output, you can use a while loop to run getline which will cause it to continue to read each line of output until it reached the end.  The below example shows y ou how to load each line of output into an array:

#+begin_src shell -n
while ("who" | getline)

    output[i++]=$0
#+end_src


Below is a pattern that allows you to manipulate the fields with-in an input line by using the ~getline~ function.  It looks for a match to "@date", and then switches it with the output of the date command:

#+begin_src shell -n
/@date/{

    "date +%T" | getline today

    gsub(/@date/,today)

}

{

    print

}
#+end_src

* *_Close And System Function_*

If I understand some what how pipes work, a file descriptor is opened to temporarily hold the data coming from one command so it can be read from another.  However, because a system can only have so many pipes open (your system is using them for other things in the background if I'm not mistaken) when working with them in awk, it's wise to close your pipes after every use.  As such, there exists a function called ~close~ that lets you do just that.  You use it by giving it an argument the pipe you used that you want to close:

#+begin_src shell -n
awk 'BEGIN{"echo what" | getline output; print output; close("echo what"}'
#+end_src

There is also a function called "system."  It allows you to execute a command supplied as an expression but does not make the output available to you.  Instead, awk waits until that command finishes executing before it proceeds with the next operation.  It also returns the exit status of the command that was executed.  (0 = success, 1 = failed)

#+begin_src shell -n
awk 'BEGIN{FS=":"}/root/{system("touch /tmp/"$1"_file")}' /etc/passwd
#+end_src

If you notice, you can also pass data that exists in the awk environment to a command called in the system function.  With the above command, there will exist a file called ~root_file~ with-in the /tmp directory.  

* *_Directing Output To Files And Pipes_*

You can redirect anything from awk, to a file that exists outside of awk (on the file system) by treating that filename as an expression.
#+begin_src shell -n
awk '{print $0 > "/some/file"}'
#+end_src

You can also pipe the output of a command in awk to a command in the shell.
#+begin_src shell -n
awk '{print | "wc -l"}' ./some/file
#+end_src

The awk book states that in the above example, the ~wc -l~ command is only executed once but all "input lines" from the print command accumulate and get sent all at once to the ~wc -l~ command?  I think I understand that right, or else the ~wc -l~ command would only do a line count on a single line?  This is a good "default" behavior to know.  

The book also states how important it is to close all the pipes that get created when executing operations like the example above.  This further cements what I believe is my understanding in that, all those input lines get put into an "unnamed" pipe whose contents are then read by the ~wc -l~ command.  Cool!  Open files from "pipes" also exist when you redirect  output from a file (If I understood the book correctly?

