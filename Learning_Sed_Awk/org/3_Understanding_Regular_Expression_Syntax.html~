<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<!-- 2023-07-13 Thu 12:01 -->
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>UNDERSTANDING REGULAR EXPRESSIONS SYNTAX</title>
<meta name="generator" content="Org mode" />
<meta name="author" content="Jessie Saenz a.k.a. Chunky_Pie" />
<style type="text/css">
 <!--/*--><![CDATA[/*><!--*/
  .title  { text-align: center;
             margin-bottom: .2em; }
  .subtitle { text-align: center;
              font-size: medium;
              font-weight: bold;
              margin-top:0; }
  .todo   { font-family: monospace; color: red; }
  .done   { font-family: monospace; color: green; }
  .priority { font-family: monospace; color: orange; }
  .tag    { background-color: #eee; font-family: monospace;
            padding: 2px; font-size: 80%; font-weight: normal; }
  .timestamp { color: #bebebe; }
  .timestamp-kwd { color: #5f9ea0; }
  .org-right  { margin-left: auto; margin-right: 0px;  text-align: right; }
  .org-left   { margin-left: 0px;  margin-right: auto; text-align: left; }
  .org-center { margin-left: auto; margin-right: auto; text-align: center; }
  .underline { text-decoration: underline; }
  #postamble p, #preamble p { font-size: 90%; margin: .2em; }
  p.verse { margin-left: 3%; }
  pre {
    border: 1px solid #ccc;
    box-shadow: 3px 3px 3px #eee;
    padding: 8pt;
    font-family: monospace;
    overflow: auto;
    margin: 1.2em;
  }
  pre.src {
    position: relative;
    overflow: auto;
    padding-top: 1.2em;
  }
  pre.src:before {
    display: none;
    position: absolute;
    background-color: white;
    top: -10px;
    right: 10px;
    padding: 3px;
    border: 1px solid black;
  }
  pre.src:hover:before { display: inline; margin-top: 14px;}
  /* Languages per Org manual */
  pre.src-asymptote:before { content: 'Asymptote'; }
  pre.src-awk:before { content: 'Awk'; }
  pre.src-C:before { content: 'C'; }
  /* pre.src-C++ doesn't work in CSS */
  pre.src-clojure:before { content: 'Clojure'; }
  pre.src-css:before { content: 'CSS'; }
  pre.src-D:before { content: 'D'; }
  pre.src-ditaa:before { content: 'ditaa'; }
  pre.src-dot:before { content: 'Graphviz'; }
  pre.src-calc:before { content: 'Emacs Calc'; }
  pre.src-emacs-lisp:before { content: 'Emacs Lisp'; }
  pre.src-fortran:before { content: 'Fortran'; }
  pre.src-gnuplot:before { content: 'gnuplot'; }
  pre.src-haskell:before { content: 'Haskell'; }
  pre.src-hledger:before { content: 'hledger'; }
  pre.src-java:before { content: 'Java'; }
  pre.src-js:before { content: 'Javascript'; }
  pre.src-latex:before { content: 'LaTeX'; }
  pre.src-ledger:before { content: 'Ledger'; }
  pre.src-lisp:before { content: 'Lisp'; }
  pre.src-lilypond:before { content: 'Lilypond'; }
  pre.src-lua:before { content: 'Lua'; }
  pre.src-matlab:before { content: 'MATLAB'; }
  pre.src-mscgen:before { content: 'Mscgen'; }
  pre.src-ocaml:before { content: 'Objective Caml'; }
  pre.src-octave:before { content: 'Octave'; }
  pre.src-org:before { content: 'Org mode'; }
  pre.src-oz:before { content: 'OZ'; }
  pre.src-plantuml:before { content: 'Plantuml'; }
  pre.src-processing:before { content: 'Processing.js'; }
  pre.src-python:before { content: 'Python'; }
  pre.src-R:before { content: 'R'; }
  pre.src-ruby:before { content: 'Ruby'; }
  pre.src-sass:before { content: 'Sass'; }
  pre.src-scheme:before { content: 'Scheme'; }
  pre.src-screen:before { content: 'Gnu Screen'; }
  pre.src-sed:before { content: 'Sed'; }
  pre.src-sh:before { content: 'shell'; }
  pre.src-sql:before { content: 'SQL'; }
  pre.src-sqlite:before { content: 'SQLite'; }
  /* additional languages in org.el's org-babel-load-languages alist */
  pre.src-forth:before { content: 'Forth'; }
  pre.src-io:before { content: 'IO'; }
  pre.src-J:before { content: 'J'; }
  pre.src-makefile:before { content: 'Makefile'; }
  pre.src-maxima:before { content: 'Maxima'; }
  pre.src-perl:before { content: 'Perl'; }
  pre.src-picolisp:before { content: 'Pico Lisp'; }
  pre.src-scala:before { content: 'Scala'; }
  pre.src-shell:before { content: 'Shell Script'; }
  pre.src-ebnf2ps:before { content: 'ebfn2ps'; }
  /* additional language identifiers per "defun org-babel-execute"
       in ob-*.el */
  pre.src-cpp:before  { content: 'C++'; }
  pre.src-abc:before  { content: 'ABC'; }
  pre.src-coq:before  { content: 'Coq'; }
  pre.src-groovy:before  { content: 'Groovy'; }
  /* additional language identifiers from org-babel-shell-names in
     ob-shell.el: ob-shell is the only babel language using a lambda to put
     the execution function name together. */
  pre.src-bash:before  { content: 'bash'; }
  pre.src-csh:before  { content: 'csh'; }
  pre.src-ash:before  { content: 'ash'; }
  pre.src-dash:before  { content: 'dash'; }
  pre.src-ksh:before  { content: 'ksh'; }
  pre.src-mksh:before  { content: 'mksh'; }
  pre.src-posh:before  { content: 'posh'; }
  /* Additional Emacs modes also supported by the LaTeX listings package */
  pre.src-ada:before { content: 'Ada'; }
  pre.src-asm:before { content: 'Assembler'; }
  pre.src-caml:before { content: 'Caml'; }
  pre.src-delphi:before { content: 'Delphi'; }
  pre.src-html:before { content: 'HTML'; }
  pre.src-idl:before { content: 'IDL'; }
  pre.src-mercury:before { content: 'Mercury'; }
  pre.src-metapost:before { content: 'MetaPost'; }
  pre.src-modula-2:before { content: 'Modula-2'; }
  pre.src-pascal:before { content: 'Pascal'; }
  pre.src-ps:before { content: 'PostScript'; }
  pre.src-prolog:before { content: 'Prolog'; }
  pre.src-simula:before { content: 'Simula'; }
  pre.src-tcl:before { content: 'tcl'; }
  pre.src-tex:before { content: 'TeX'; }
  pre.src-plain-tex:before { content: 'Plain TeX'; }
  pre.src-verilog:before { content: 'Verilog'; }
  pre.src-vhdl:before { content: 'VHDL'; }
  pre.src-xml:before { content: 'XML'; }
  pre.src-nxml:before { content: 'XML'; }
  /* add a generic configuration mode; LaTeX export needs an additional
     (add-to-list 'org-latex-listings-langs '(conf " ")) in .emacs */
  pre.src-conf:before { content: 'Configuration File'; }

  table { border-collapse:collapse; }
  caption.t-above { caption-side: top; }
  caption.t-bottom { caption-side: bottom; }
  td, th { vertical-align:top;  }
  th.org-right  { text-align: center;  }
  th.org-left   { text-align: center;   }
  th.org-center { text-align: center; }
  td.org-right  { text-align: right;  }
  td.org-left   { text-align: left;   }
  td.org-center { text-align: center; }
  dt { font-weight: bold; }
  .footpara { display: inline; }
  .footdef  { margin-bottom: 1em; }
  .figure { padding: 1em; }
  .figure p { text-align: center; }
  .equation-container {
    display: table;
    text-align: center;
    width: 100%;
  }
  .equation {
    vertical-align: middle;
  }
  .equation-label {
    display: table-cell;
    text-align: right;
    vertical-align: middle;
  }
  .inlinetask {
    padding: 10px;
    border: 2px solid gray;
    margin: 10px;
    background: #ffffcc;
  }
  #org-div-home-and-up
   { text-align: right; font-size: 70%; white-space: nowrap; }
  textarea { overflow-x: auto; }
  .linenr { font-size: smaller }
  .code-highlighted { background-color: #ffff00; }
  .org-info-js_info-navigation { border-style: none; }
  #org-info-js_console-label
    { font-size: 10px; font-weight: bold; white-space: nowrap; }
  .org-info-js_search-highlight
    { background-color: #ffff00; color: #000000; font-weight: bold; }
  .org-svg { width: 90%; }
  /*]]>*/-->
</style>
<link rel="stylesheet" type="text/css" href="../../CSS/gray.css"
<script type="text/javascript">
// @license magnet:?xt=urn:btih:e95b018ef3580986a04669f1b5879592219e2a7a&dn=public-domain.txt Public Domain
<!--/*--><![CDATA[/*><!--*/
     function CodeHighlightOn(elem, id)
     {
       var target = document.getElementById(id);
       if(null != target) {
         elem.classList.add("code-highlighted");
         target.classList.add("code-highlighted");
       }
     }
     function CodeHighlightOff(elem, id)
     {
       var target = document.getElementById(id);
       if(null != target) {
         elem.classList.remove("code-highlighted");
         target.classList.remove("code-highlighted");
       }
     }
    /*]]>*///-->
// @license-end
</script>
</head>
<body>
<div id="content">
<h1 class="title">UNDERSTANDING REGULAR EXPRESSIONS SYNTAX</h1>
<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgd3db64a"><b><span class="underline">Character Classes</span></b></a></li>
<li><a href="#org7539d19"><b><span class="underline">POSIX Character Class Additions</span></b></a></li>
<li><a href="#orgd9ffc97"><b><span class="underline">Repeated Occurences Of A Character</span></b></a></li>
<li><a href="#org47aeff4"><b><span class="underline">Grouping Operations</span></b></a></li>
<li><a href="#orgd32e769"><b><span class="underline">Matching The Shortest Pattern</span></b></a></li>
<li><a href="#orgb245cb1"><span class="underline"><b>Handy Regex's</b></span></a></li>
</ul>
</div>
</div>

<div id="outline-container-orgd3db64a" class="outline-2">
<h2 id="orgd3db64a"><b><span class="underline">Character Classes</span></b></h2>
<div class="outline-text-2" id="text-orgd3db64a">
<p>
<code>character class</code> - allows you specify a list of characters to be matched and any of these characters can occupy a single position.  
</p>

<p>
One method for using character classes that I didn't think of but is an obvious use case and demonstrated in so many of the books I've been
reading is like below:
</p>

<div class="org-src-container">
<pre class="src src-shell">\.H[123456]
</pre>
</div>

<p>
The above will match any markdown heading that ends with either a 1,2,3,4,5 or 6.  The book also gives an example that can be used with grep
for searching notes in a chapter!
</p>

<div class="org-src-container">
<pre class="src src-shell">grep 'the' Ch[0-9]
</pre>
</div>

<p>
The following are a list of special characters that have special meaning with-in a character class:
</p>

<p>
<code>\</code> - Escapes any special character (awk only. - i.e. if you wanted to escape a bracket)
<br />
<code>-</code> - Indicates a range when not in the first or last position
<br />
<code>^</code> - Indicates a reverse match only when in the first position.
<br />
</p>


<p>
It's important to remember that each character with-in a <code>character class</code> will match only against a single character in your query.  For example if you have a text file with only a single word in it, 'Linux', then it will examine that word, one character at a time to see if it matches any of the characters in your <code>character class</code>.
</p>

<ul class="org-ul">
<li>Linux would be L | i | n | u | x</li>
</ul>

<p>
And if your character class was <code>[a-z]</code>, then it would start with examing 'L' and seeing if it matches any character in the class.  Since the characters are all lower case letters between a and z, it would NOT find a match, see!
</p>

<p>
Below are characters that have special meaning with-in character class syntax but can be a character in of itself with-in a class to be matched
provided it meets these special circumstances:
</p>

<p>
<code>]</code> - Can be used as a character to match only if it is the first character in the class.  i.e. [[abc]
<code>-</code> - Can also be used as a character to match only if it is the first character in the class.  i.e. [-abc]
</p>

<p>
In awk, you would simply escape these characters to use them as matches.  
</p>

<p>
The special character <code>^</code> when the first character with-in a character class is used as a NOT operator and therefore negates any match for the characters
with-in the character class.  
</p>
</div>
</div>

<div id="outline-container-org7539d19" class="outline-2">
<h2 id="org7539d19"><b><span class="underline">POSIX Character Class Additions</span></b></h2>
<div class="outline-text-2" id="text-org7539d19">
<p>
With-in the POSIX standard there exist the concept of basic regular expressions (BRE's) and extended regular expressions (ERE's.)  The former are what are used with-in grep and sed and the latter are what are used in awk.  I have no idea what consists in each but thought it was worth documenting here.
</p>

<p>
With the posix standard is also the concept of calling what previously in these notes were named "character classes", as "bracket expressions.  This is because in addition to the characters that you can put with-in a "bracket expression" like "a-z0-9" or "lmnop" you have a grouping of characters known in the posix standard as "character classes" which  allow you to specify a specific class of characters to match against.  i.e. - digits, alpha, alpha-numberic, blanks, etc, etc.
</p>

<p>
These posix standard character classes are enclosed with-in this special syntax [: <code>and</code> :] There are other special syntax that can match items
unique to non-english languages that I'm personally not concerned about.  Below are the posix standard character classes:
</p>

<p>
[:alnum:] - all printable characters, including white-space.
<br />
[:alpha:] - alphabetic characters
<br />
[:blank:] - space and tab characters
<br />
[:cntrl:] - non-printable characters i.e. - lf, crlf
<br />
[:digit:] - numberic characters
<br />
[:graph:] - printable and visible non-space characters
<br />
[:lower:] - lowercase characters
<br />
[:print:] - printable characters including whitespace
<br />
[:punct:] - punctuation characters
<br />
[:space:] - whitepsace characters<br />
<br />
[:upper:] - uppercase<br />
[:xdigit:] - hexadecimal digits
<br />
</p>
</div>
</div>

<div id="outline-container-orgd9ffc97" class="outline-2">
<h2 id="orgd9ffc97"><b><span class="underline">Repeated Occurences Of A Character</span></b></h2>
<div class="outline-text-2" id="text-orgd9ffc97">
<p>
<code>*</code> - this special character is used to denote zero or more instances.
</p>

<p>
For example, <code>[0-9]*</code> would mean to say zero or more of any number.
</p>

<p>
NOTE: Remember how literal regex's can be.  The only thing that will be matched by the <code>*</code> special character is the character immediately preceeded
by it.  i.e. - [15]00* will not match zero or more instances of 00 but only zero or more 0's preceeded by a literal 0.  Hope this makes sense.  
</p>

<p>
This can also be used to your advantage let say, if you're looking to match spaces but don't know if the spaces will be single or many.  You can
use the expression '  *' to mean mean match any spaces, whether it be a single space or a space follow by zero or more spaces.  Again, hope this makes
sense.
</p>

<p>
The technical term of matching zero or more of something is called "closure."  There exist more variations of closure in ERE style expressions like so:
</p>

<p>
<code>+</code> - Allows you to match 1 or more of something.
<code>?</code> - matches zero or only 1 of something
</p>

<p>
<b><span class="underline">Positional Metacharacters</span></b>
</p>

<p>
There exist more special characters that allow you to specify that position of a match with-in a line.
</p>

<p>
<code>^</code> - allows you to specify a match at the beginning of the line. i.e. ^[0-9] matches any number at the beginning of a line
<br />
<code>$</code> - matches at the ending of a line. i.e. [0-9]? matches any number at the very ending of a line
<br />
A clever use of these positional metacharacters can be <code>^$</code> to match an empty line. (nothing inbetween the beginning and ending).  These metacharacters
are known as "anchors."
</p>

<p>
In BRE's, the above "achors" are only special if they are at their respective "beginning" or "ending" of a regular expression.  Otherwise, they're matched literally.
</p>

<p>
<b><span class="underline">A Span Of Characters</span></b>
</p>

<p>
{} - is a construct that allows you to specify a range of characters to match.
<br />
These exist with-in BRE's but must be escaped as such \{ \}.  
</p>
</div>
</div>


<div id="outline-container-org47aeff4" class="outline-2">
<h2 id="org47aeff4"><b><span class="underline">Grouping Operations</span></b></h2>
<div class="outline-text-2" id="text-org47aeff4">
<p>
Grouping operations can be thought of as opposites to character classes.  Where character classes provide a match to any
character with-in that class, a grouping operation provides a match to the entire grouping of characters.  To use this in a BRE, you must escape the special grouping characters.
</p>

<p>
[aeiou] will match a one of the letters
</p>

<p>
(ought) will match any to those exact grouping of letters anywhere.  i.e. thought, fought
</p>

<p>
You can enclose the or operator with-in a group operation like so:
</p>

<ul class="org-ul">
<li>(ought|aught) will match caught or thought</li>
</ul>
</div>
</div>

<div id="outline-container-orgd32e769" class="outline-2">
<h2 id="orgd32e769"><b><span class="underline">Matching The Shortest Pattern</span></b></h2>
<div class="outline-text-2" id="text-orgd32e769">
<p>
When using regex to match a pattern, it will return the longest match to that pattern.  Sometimes we want the first match to the pattern, regardless if it's shorter than the longest match.
</p>

<p>
One way to accomplish this is to include a ! match for a character that exists in the longest match to that pattern.  For example, lets say you 
have a document that has strings enclosed in single quotes:
</p>

<p>
'dad' 'mom' 'first son' 'first daughter' 'second son' 'puppy'
</p>

<p>
You want to return the first match for a pattern that is a string with-in single quotes.  You can use the regex of 
</p>

<div class="org-src-container">
<pre class="src src-shell">grep -wo "'.*'" document

'dad' 'mom' 'first son' 'first daughter' 'second son' 'puppy'
</pre>
</div>

<p>
but it will return the entire line because your matching zero or more of any character(which includes single quote) inbetween two literal single
quotes.  So how do you include a ! match for a character that exists in the longest match to that pattern?  You find the one character that exists in the longest match to the pattern (which is the entire line) but not in the shortest match which is a string with-in single quotes and include it in a character class with the <code>^</code> to negate a match for it and follow that character class with a <code>*</code> closure to match zero or more occurances of any character <i>OTHER</i> than the character you negated a match for.  Like so:
</p>

<div class="org-src-container">
<pre class="src src-shell">grep -wo "'[^']*'" document

'dad'
'mom'
'first son'
'first daughter'
'second son'
'puppy'
</pre>
</div>

<p>
Another subtlety of regex's is the concept of "anchoring."  If you account for the characters the come before and after your match, then your regexwon't catch the match that exists at the beginning and ending of a line unless you use the <code>^</code> and <code>$</code> anchors.  
</p>

<p>
Alternatively, if you <i>don't</i> account for the characters that come before and after your match, it will only return the first match that exists 
either at the beginning or end!
</p>

<p>
The above observations aren't fully baked and are only the patterns I see so far as I continue to learn regular expressions.
</p>
</div>
</div>

<div id="outline-container-orgb245cb1" class="outline-2">
<h2 id="orgb245cb1"><span class="underline"><b>Handy Regex's</b></span></h2>
<div class="outline-text-2" id="text-orgb245cb1">
<p>
<code>^ *$</code> - matches any blank line that may contain spaces in it
</p>


<p>
The regex that is matching your pattern is likely matching a repeating character.  To return the shortest match to that pattern, you would want to include a construct that says, return anything "but that repeating character".  i.e. [^/]
</p>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="date">Date: 2023-02-10 Fri 00:00</p>
<p class="author">Author: Jessie Saenz a.k.a. Chunky<sub>Pie</sub></p>
<p class="date">Created: 2023-07-13 Thu 12:01</p>
<p class="validation"><a href="https://validator.w3.org/check?uri=referer">Validate</a></p>
</div>
</body>
</html>
