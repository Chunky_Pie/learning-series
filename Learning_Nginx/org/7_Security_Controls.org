#+Title: Security Controls
#+Author: Jessie Saenz a.k.a chunky_pie
#+Date: 10-11-23
#+OPTIONS: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../CSS/gray.css"

* *_Access Based On IP Address_*

You can either by the HTTP or Stream modules, limit access to a resource by IP Address.  There exist directives that allow you specify whether to ~deny~ or ~allow~ based on IP4 or IP6 Addresses or by entire Subnets.

#+begin_src
  location /admin/ {

      deny 10.0.0.1;
      allow 10.0.0./20;
      allow 2001:0db8::/32;
      deny all;

  }
#+end_src

The above directives can be placed with http, server and location contexts in addition stream contexts for UDP/TCP.  Any requests that fall outside the rules set by these directives will return a 403 response.

* *_Allowing Cross-Origin Resource Sharing_*

This section is to advanced for me.  I still have no clue about setting headers to understand this section.

* *_Client-Side Encryption_*

The following directives apply with-in HTTP and Stream contexts and allow you to encrypt traffic between your Nginx server and the client (~ngx\under{}http\under{}ssl\under{}module~ or ~ngx\under{}stream\under{}ssl\under{}module~).

#+begin_src
  server {

      listen 8443 ssl;
      ssl\under{}certificate /etc/letsencrypt/live/chunkypie.me/fullchain.pem;
      ssl\under{}certificate\under{}key /etc/letsencrypt/live/chunkypie.me/privkey.pem;
    
  }
#+end_src

The ~ssl~ parameter further defines the ~listen~ directive to use tls encryption on port 8443. The ~ssl\under{}certificate~ directive takes a filename path as an argument which is the path to a certificate.  The ~ssl\under{}certificate\under{}key~ directive also takes a filename path as an argument but is the path to a certificate key.  

* *_Advanced Client-Side Encryption_*

The SSL modules for stream and HTTP allow for granular controls of how traffic is encrypted in Nginx.  Below are the available directive and what they offer:

- ssl\under{}protocols - Takes as an argument a space seperated list of SSL Protocol versions accepted.
- ssl\under{}ciphers - Takes as an argument a colon seperated list of the SSL ciphers accepted.  The ciphers must be "cipher strings" documented in ~openssl-ciphers~ man page.  You can negate that a specific cipher be used by using the bang (!) character

#+begin_src
server {
    listen 8443 ssl;

    ssl\under{}protocols TLSv1.2 TLSv1.3;
    ssl\under{}ciphers HIGH:!aNULL:!MD5;

    ssl\under{}certificate /etc/nginx/ssl/example.crt;
    ssl\under{}certificate\under{}key /etc/nginx/ssl/example.pem;

    ssl\under{}certificate $ecdsa\under{}cert;
    ssl\under{}certificate\under{}key data:$ecdsa\under{}key\under{}path;

    ssl\under{}session\under{}cache shared:SSL:10m;
    ssl\under{}session\under{}timeout 10m;
}
#+end_src

You can also provide multiple paths to ssl certificates and ssl keys.  The logic being you can configure a more efficient/securer, albiet less supported encryption algorithm in addition to a more standard and widely one and the one that is capable by the client and accepted by the server will be used.  As you see in the second ~ssl\under{}certificate\under{}key~ directive, you can also use a variable to provide the ssl key.  However, the value to variable can also be the actual /key/ and not just the path to it.  If you prefix the variable with ~data:~ it tells Nginx that the value of the variable is the keys actual direct value.

- ssl\under{}session\under{}cache - Takes as an argument one of two types of ssl session caches and their sizes used to store session parameters.  One is per worker process and one is shared between all worker process.
- ssl\under{}session\under{}timeout - Takes a time span as an argument and whose value specifies how long a client may reuse the session parameters in the cache.

* *_Upstream Enctryption_*

Previous sections have applied to encrypting traffic between the client and Nginx.  The following directives allow you to encrypt traffic between Nginx and upstreams:

- proxy\under{}ssl\under{}verify - Takes on or off as an argument to dictate wether proxied HTTPS server certificate are verified or not.
- proxy\under{}ssl\under{}cverify\under{}depth - Takes an integer as a value to defines how many levels deep should a certificate be validated. 
- proxY\under{}ssl_protocols - Takes multiple tls versions as arguments that specify what protocols to accept when connecting to an upstream

By default, Nginx doesn't verify upstream certificates and accepts all TLS versions.  In addition to the above directives, you can use the proxy versions of ~ssl_certificate~ and ~ssl_certificate_key~ to specify the path of a certificate and key to use for authentication to upstream servers.

* *_Securing A Location_*

There exists a secure link module which includes the ~secure_link_secret~ directive used to restrict access to a resource, to users who have a secure link.

#+begin_src
location /resources {

    secure_link_secret chunkyandfunky;
    if ($secure_link = "") { return 403; }
    rewrite ^ /secured/$secure_link;
    
    }
    
location /secured/ {

    internal;
    root /var/www;
    
}
#+end_src

The above config exposes ~/resources~ to the public but evaluates the ~$secure_link~ variable when a request for a resource there is made and only if that variable evalutes to a non-empty value will it be sent to the real location of that resource, expressed in the ~/secured/ location  block.

Lets say the resource we want is ~secret.html~ located at www.chunkypie.me/resources and the /secret/ is chunkyandfunky.  You would put  ~secret.html~ and chunkyandfunky together (resource+secret) as a single string and create a hash from it.

#+begin_src shell -n
echo "secret.htmlchunkyandfunky" | md5sum

6b516dfd3265f37bb245f6de1098e166
#+end_src

You now make a request using that hash, followed by the original URI, www.chunkypie.me/resources/6b516dfd3265f37bb245f6de1098e166/secret.html.
When Nginx receives this request, it takes the URI and attempts to match it to a location block with-in it's config.  In this case, it best matches the ~/resources~ location block.  Because it knows the /secret/ via the  ~secure_link_secret~ directive, it then uses it along with the URI (~secret.html~) to create a hash and if it matches the hash included in the request, it fills the ~$secure_link~ variable with the URI.  If it doesn't match then ~$secure_link~ evalutes to an empty string and as such returns a 403 error.

As you can see in the config above, there includes a ~rewrite~ directive that appends the ~$secure_link~ to a =/secured/= location which when evaluated will match the =/secured= location block and in turn will return the resource that exists there.

NOTE: The argument passed to the ~secure_link_secret~ directive must be a string literal, it cannot be a variable.

* *_Securing A Location With An Expire Date_*

This section not only covers how to set an expiration for a secured link but also on how to dynamically create the secured link and hash using the values of variables that are initialized by other means.  There are directive that are used to accomplish this explained below.

~secure_link~ - Takes two parameters.  The first is the actual md5 hash (this can be an http argument/variable for example) and the second parameter is when this link expires (must be in epoch format and can also be an http argument/variable).

~secure_link~md5~ - This takes only one parameter which is the format of the string that is used to construct the md5 hash.  As you can see in the example, the format is the lifetime of the secure link, the uri, the clients address and the actual secret.  The intent is to compose the format of the string using as many embedded variables as necessary to strengthen your security.

#+begin_src
  location /resources {

      root /var/www;
      secure_link $arg_md5,$arg_expires;
      secure_link_md5 "$secure_link_expires$uri$remote_addrmySecret";
      if ($secure_link = "") { return 403; }
      if ($secure_link = "0") { return 410; }
	 
  }
#+end_src
 
* *_Generating An Expiring Link_*

The hash that is created using the pattern given as an argument to the ~secure_link_md5~ directive, has to be in binary format.  I don't understand at moment why it has to be in binary format but in addition, it must be encoded using base64 and any "+" or "=/=" characters that exist in the resulting base64 encoding must be transformed to "-" and "_" characters respectively, any equal signs must be deleted.  This is so they're not confused for url encoding special characters.  

So the first step in generating an expiring link is to convert a human readable date into epoch seconds:
#+begin_src shell -n
date -d "today" +%s

1698892122
#+end_src

Next is concatenating your string that matches the pattern indicated in your ~secure_link_md5~ directive:
#+begin_src
1698892122/resources/index.html127.0.0.1mysecret
#+end_src

Lastly, you use whatever tools you have at your disposal to create a binary hash of that string, base64 encode it and remove any "=" characters in addition to translating "+" and "/" to "-" and "_"'s.  Because I only know bash (the book shows how this can be done in Python and bash) I'm going to use shell commands to do this:
#+begin_src shell -n
  echo '1698892122/resources/index.html127.0.0.1mysecret' \
  | openssl md5 -binary \
  | base64 \
  | tr +/ -_ \
  | sed 's/=//g'

  2XIfnouuX_OR2XVeGtY3oA
#+end_src

I still have yet to understand how we can control who gets access to a secure location and who does not using the above sections but....maybe later in my reading of this book I will discover.  

* *_HTTPS Redirects_*

There exist a way to redirect traffic coming in on one port, to https.

#+begin_src
server {

    listen 80 default_server;
    listen [::]:80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
    
}
#+end_src

IPV6 addresses are defined with-in square brackets so the second ~listen~ directive is matching all IPV6 address.  Although the ~server~ directive isn't needed since we've already established that this server block is the default for traffic on port 80, it's listed here with the name ~_~ which simply serves as a catch all for any hostname since non will likely match ~_~.

Last is the ~return~ directive.  There are various ~return~ directives depending on the module.  This instance uses the directive from the ~http_rewrite_module~ which takes as a mandatory argument, the desired code to return to the client and if either codes 301, 302, 303, 307 or 308 are returned, you can specify a redirct URL.  If any other code is returned you can return a body of text as an a secondary argument.

* *_Redirecting To HTTPS Where SSL/TLS Is Terminated Before Nginx_*


