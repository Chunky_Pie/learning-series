#+Title: Traffic Management
#+Author: Jessie Saenz a.k.a chunky_pie
#+Date: <2023-09-07 Thu>
#+options: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../CSS/gray.css"

* *_A/B Testing_*

There exists an Nginx module that allows you to split traffic based on a factor of your choice between two different destinations, two different resources, or two of anything that Nginx is sendings requests to or returning as a response.  That module is called the ~split_clients~ module.

This module works by giving the ~split_clients~ directive two arguments that will be used in a function to assign a value to a variable (your second argument.)  The first argument is a key that will be used in a hash function and like the LB hash function, can be anything (string literal, variable[s] or any combination of).  I believe this hash will be of your key and the request so it's exclusive to each request.  The second argument is a variable name.  Lastly is the function which consists of a key-value pair that defines what percentage of your requests will go to one of two values that will be either destinations|resources|etc.  Because you're splitting requests by two, one key will be a percentage and the other key will be an astericks (which denotes whats left after the percentage.

#+begin_src
split_clients "${remote_addr}AAA" $variant {

    20.0% "backendv2";
    *     "backendv1";
      
}
#+end_src

The above examples uses the remote address of the client with 'AAA' appended to it as the key for the hash function and the $variant variable will be filled with "backendv2" for 20% of the total traffic with "backendv1" for the remaining traffic.

#+begin_src
location / {

    proxy_pass https://$variant

}
#+end_src

And as you can see, you now use that variable with other Nginx constructs which in this case will send traffic based on the percentages you defined with-in the ~splits_clients~ directive.  Cool huh!

* *_Using GeoIP Module_*

What if you wanted to make decisions based on where the client was located geographically?  Well, that is accomplished via the ~GeoIP~ module.  However, this is a module that is likely not compiled by default in your Nginx installation.  You can check if it is by running the following command: ~nginx -V 2>&1 | egrep -i 'geo'~ .  If not, you can search for it as a package and install it (it's called nginx-module-geoip).  Secondly, you also have to download and extract the databases for all the countries and cities into a folder that you will reference later.  Lastly, you have to program nginx to load that module and tell it where these country and city databases are. You do so using the ~load_module~ directive which can only be defined with the main context of ~nginx.conf~ and because the geoIP module only works for HTTP traffic, you specify with-in the HTTP block the location of your databases:

#+begin_src
load_module "/usr/lib64/nginx/modules/ngx_http_geoip_module.so";

http {

    geoip_country /etc/nginx/geoip/GeoIP.dat;
    geoip_city /etc/nginx/geoip/GeoLiteCity.dat;
    # ...

}
#+end_src

The geoIP module exposes two directives ~geoip_country~ and ~geoip_city~ which themselves make available various embedded variables.  Below are these directive with their respective available parameters:

_geoip\under{}country_
- $geoip\under{}country\under{}code
  * returns a country's two letter country code
- $geoip\under{}country\under{}code3
  * returns a country's three letter country code
- $geoip\under{}country\under{}name
  * returns the country's full name

_geoip\under{}city_
- This directive exposes /a lot/ of variables, including the same as ~geoip_country~ but with the word "city" in the name.  i.e. - ~geoip_city_country_code~, etc, etc.  Although the list of available variables are to long to list and describe here, just know that you can use these variables to describe the geographical location of clients with-in your logs to make decisions on how to route traffic.  i.e. (the map function explained in earlier chapters)

* *_Restricting Access Based On Country_*

In this section we basically give an example of how to use a map function and */NEW/* to my notes, how to use an *IF* statement to execute something based on the condition of a country.  First the map function!

Although mentioned in previous sections, I don't think I understood how to use the map function until now.  It's quit simple and reminds me of bashs' =case= construct.

#+begin_src
map $geoip_country_code $dest {

    "US"    0;
    default 1;

}
#+end_src

The above example translates to "by default, the value of $dest is 1 but if $geoip_country_code equals US, then the value of $dest is 0".  You can include as many =key    value= pairs as you cant.  The =default= key is a keyword that allows you to assign a value to the variable if no other key is matched.

Lastly, you can use an *IF* statement to execute any logic you want based on the value of =$dest=.  The syntax to me based on what I know is akin to how conditionals are expressed in the Awk programming language.

#+begin_src
server {

    if ($dest = '1') {

        return 403;

    }

    ....

}
#+end_src

I'm sure you can pretty much define anything you would normally define with-in a server block using the *IF* statement?  I'll have to come back and confirm after I get more hands on!

* *_Finding The Original Client_*

It seems the geoIP module has more tricks up it's sleeve than just identifying countries!  You can use other directives to define what IP ranges are those that belong to your internal proxies (known as trusted addresses) and search for the real source IP of the client (since as a packet traverses the network, the source address will always be that of the last proxy).

#+begin_src
http {

    geoip_country /etc/nginx/geoip/GeoIP.dat;
    geoip_city /etc/nginx/geoip/GeoLiteCity.dat;
    geoip_proxy 10.0.16.0/26;
    geoip_proxy_recursive on;
    # ...
    
}
#+end_src

So just like before, you define the locations of your database files with-in the HTTP config because this module only applies to HTTP traffic but as you can see, the ~geoip_proxy~ directive takes as an argument the network name (in CIDR notation) where your proxy's live and the ~geoip_proxy_recursive~ directive takes as it's argument whether you can the feature on or off.

Without it on, and this isn't explicitly explained in the book but any system variables for a clients ip address would have the last IP address that packet came from as it's value which would likely be from one of your proxies.  If you turn it on, it causes Nginx to traverse the "X-Forarded-For" header to find the last client IP known (which has a good chance of being the true client source IP).

*NOTE*:
There exists in the HTTP protocol a header named "Forwarded" which has become the standard header for adding proxy information for proxied requests.  However, the header used by Nginx is the "X-Forwarded-For" which although not an official standard, is still widely used, accepted and set by most proxies.

* *_Limiting\under{}Connections_*

You can limit the amount of connections a client makes using the ~limit_conn_module~ .  It works by taking a key parameter from the user which is the factor you want to consider when limiting your connections (in the book it used the client IP in a binary format) and setting aside a memory zone to store this information.  You then specify that memory zone and the amount of connections you want to limit.  Below are the directives and parameters associated with the ~limit_con_module~ and a brief explanation of each:

_limit\under{}conn\under{}zone_\\
This is the main directive which takes two parameters.
 - the factor your basing your connection limits on (i.e. - client ip, geo ip, etc, etc)
 - a zone definition using the =zone=name:size= format. (i.e. - zone=myzone:1m)

_limit\under{}conn\under{}status_\\
This takes as an argument the HTTP response code you will give users once they've reached their connection limit (i.e. 429)

_limit\under{}conn_\\
This is the directive you will use to actually institute the connection limited and it takes two arguments.
 - zone name
 - an integer that is the amount of connections you want to limit by (i.e. 40)

#+begin_src
http {
 
   limit_conn_zone $binary_remote_addr zone=limitbyaddr:10m;
   limit_conn_status 429;
   # ...

   server {
 
       # ...
       limit_conn limitbyaddr 40;
       # ...
 
   }
 
}
#+end_src

As is it's been a theme, we're configuring the main directive (connection limiting) with-in the higher level HTTP block and we're instituting the ~limit_conn~ directive and defining the limit of connections in the appropriate scope for our needs.  You can define your ~limit_conn~ and actually even ~limit_conn_status~ in http, server or location contexts!

As it's probably obvious by now, this module is a great candidate for defending against ddos attacks and if combined with a factor that allows you to be more granular on who you're limiting (i.e. session cookie vs remote IP) it can be effective while at the same time not block unintended traffic (i.e. - all hosts behind a NAT which are all using the same source IP of the router).

Lastly, there exists an embedded variable you can include in your access logs to see whether requests were "PASSED, DELAYED or REJECTED."  This variable is called ~$limit_req_status~.

Whats cool about this module is there exists a ~limit_req_dry_run~
directive which takes "on" as an argument and when activated allows
the ~$limit_req_status~ variable to be evaluated to
"DELAYED\under{}DRY\under{}RUN or REJECTED\under{}DRY\under{}RUN" for
connections where a limit was reached.  This way, you can see how your
configuration is affecting traffic and make tweaks as needed before
going live (setting ~limit_req_dry_run~ to off/removing).

* *_Limiting Rate_*

Like with connections, Nginx has a module available that allows you to limit how many requests a client can make with-in a given period.  That module is called ~limit_req~ and it works pretty similiar to the ~limit_conn~ except it has an extra paramter that allows you set the rate ratio (i.e. /N/ requests per second).  Below are the associated directives and parameters for ~limit_req~ :

_limit\under{}req\under{}zone_\\
This is the main directive which takes three parameters
- the factor your basing your rate limits on (i.e. - client ip, session cookie)
- a zone definition using the =zone=name:size= format (i.e. - zone=myzone:1m).
- your rate for the number of requests per second or minute, using the =rate=<integer>r/s|m= format (i.e. - rate=5r/s or rate=10r/m).
- lastly and this is for *(Nginx Plus)* only is ~sync~.  This allows you to synchronize your zone memory between nodes in an Nginx cluster. 

_limit\under{}req\under{}status_\\
This takes as an argument the HTTP response code you will give users once they've reached their rate limit (i.e. 429)

_limit\under{}req_\\
This is the directive you will use to actually institute the rate limit and it takes one /required/ parameter and three  optional ones:
- the name of your zone, accept it requires this format for some reason? =zone=name=
- the total number of requests over your limit that you want to allow as a sporadic "burst" and not be rejected (this total is counted over a span of a second).  It is expressed in the =burst=N= format (i.e. burst=12)
- ~nodelay~ which tells Nginx to allow all the requests defined in ~burst~ to be consumed all at once with no delay
- ~delay=N~ sets the threshold at which requests, upto the burst limit are "throttled" by a speed that allows the rate of requests to not exceed the rate limit.  

A little more explanation on the ~burst~, ~nodelay~ and ~delay~ parameters.  Even though you can set a "burst" maximum of allowed requests to be served despite a rate limit, it's technically never exceeded.  Nginx delays the sending of each request that is over the limit at a rate that ensures the limit is never exceeded.

Now, you can further control that and say "don't throttle ANY of those requests, send them all at once upto the burst max" ~nodelay~.  Or you can say, "if a burst of requests come in, let them have /N/ requests immediately upfront and throttle the rest upto the burst max" ~delay=N~.  

#+begin_src
http {

    limit_req_zone $binary_remote_addr
    zone=limitbyaddr:10m rate=3r/s;
    limit_req_status 429;
    # ...

    server {

	# ...
	limit_req zone=limitbyaddr burst=12 delay=9;p
	# ...

    }

}
#+end_src
  
* *_Limiting Bandwidth_*

Last but not least is the ability to limit the bandwidth for client requests.  This one is a little bit easier with only two directives.\\
_limit\under{}rate\under{}after\\_
- the threshold in either m or g upto where bandwidth is unlimited (i.e. =limit_rate_after 10m=)

_limit\under{}rate_
- the bandwidth rate limit in m or g, after the above threshold is passed (i.e =limit_rate 1m=)
- ~$limit_rate~
  - a special variable you can programmatically initialize to use as the value for the ~limit_rate~ directive.

#+begin_src
location /download/ {

    limit_rate_after 10m;
    limit_rate 1m;

}
#+end_src

As you can see, these two directives can be placed with in the specific context you want them applied to.

