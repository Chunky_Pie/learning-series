#+Title: Querying
#+Author: Jessie Saenz a.k.a
#+Date: <2022-10-25 Tue>
#+options: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../../CSS/gray.css"

* *_Overview_*
Prometheus uses PromQL (Prometheus Query Language) to express queries for timed series data in real-time.  You can have results shown as a graph, tabular data via Promethesus's
expression browser or consumed by an external system like Grafana.

Below are the data types used in PromQL:
- Instant Vector
  + A single sample for a time series metric.  Think of a time series as just a way to say that the metric spans over time.  Like a heart-beat monitor in an ER room as
    you lay there or the count of how many bananna's you have at home as the days pass by.  ~Instant Vector~ data type is a single sample for that metric.  i.e. what your
    heart-beat is at that point in time or how many bananna's you have at home at that point in time.  
- Range vector
  + A sample for a metric over a range of period
- Scalar
  + a number floating point value
- String
  + a simple string value; currently unused?
\\
\\
* *_Instant Vector Selectors_*

Instant vector selectors are a query that allow you to fetch a instant vector (fancy name for a single sample value) for a single time series or multiple time series.  You can use an instant vector selector to
 return a single sample value for a metric by just calling the metric name:

#+begin_src
apt_upgrades_pending
#+end_src

The above, will return a single sample value for the total number of packages pending to be updated for each machine in your configuration file. (example below)
[[./apt_packages.png]]

You can filter and narrow down that query using the same curly brace, key/value pair format.  For example, the below command will limit the same instant vector selector to
only my machine named server1 and security updates only, all while using the regex operator (operators you can use are listed in the section below outlining common operators.)
#+begin_src
apt_upgrades_pending{instance=~"monster.+", origin=~".+security"}
#+end_src

As you can see below, I can take the full results from a general instant vector selector and use it's key value pairs (known as label matchers) to further limit my result.
[[./filtered_apt_upgrades_packages.png]]


You can also use the following operaters that work just like when in bash shell:
- = (match)
- != (not match)
- =~ (regex match)
- !~ (not match regex)

NOTE:  Regex matches are fully anchored and use RE2 syntax.  Meaning if you have a single string as your regex value, it will only return true if a single line contains only that value.  
\\
\\
* *_Range Vector Selectors_*

A Range vector selector is a syntax that allows you to get multiple sample values over a specified period of time. You do this by simply adding a
time expression at the end of your instant vector selector query. This time expression would simply be a number and unit of time enclosed with-in 
brackets. For example, if you wanted to see the total of http requests over the last 10s westeconds you would use the following query:

#+begin_src shell
http_requests_total[10s]
#+end_src
[[./range_vector_10_seconds.png]]

As you can see above you get multiple results but I'm sure you're confused about what "@1666564658.118" means!  It's actually a date in unix or epoch time format and would urge
you to look [[https://www.codingem.com/epoch-time/][here]] for a primer on unix/epoch time format.  You can use the date command to convert unix time to human readable time like below:


#+begin_src shell
date -d "@1666564658.118"

Sun Oct 23 04:37:38 PM MDT 2022
#+end_src

The available time durations are as follow:

- ms - milliseconds
- s - seconds
- m - minutes
- h - hours
- d - days
- w - weeks
- y - years

You can also combine the time durations to express lets say, 5 hours and 30 minutes but they have to have no spaces.  Below are some valid examples:

#+begin_src shell
5h30m10s
1h30m
#+end_src

You can use the offset keyword to query a time in the past.  i.e. 5 minutes ago or 5 hours ago

#+begin_src shell
http_requests_total offset 5m
#+end_src

You can combine the offset with the the [N<time>] range vector selector to let say what the total http requests over a span of 5 minutes 1 week ago:
#+begin_src shell
http_requests_total[5m] offset 1w
#+end_src

You can also use the same @epoch time formate to specify a result during a specific time and date.  i.e. what was the http request total for the past 5 minutes on Sun Oct 23 04:37:38 PM MDT 2022?
#+begin_src shell
http_requests_total[5m] @ 1666564658.118
#+end_src

However, this feature isn't available right now in the current version I'm using and I believe is disabled by default on the verion it is available on which is v2.25.0.
I'm currently on v2.15.  
\\
\\
* *_Operators_*

PromQL uses multiple operators to allow you to operator on metric data, most of which are common and familiar to most IT professionals which are
the arithmetic operators:

- + - addition
- - - subtraction
- * - multiplication
- / - division
- % - modulo
- ^ - power/exponentiation

You can use these operators like so:

#+begin_src shell
http_requests_total[10s] *2
#+end_src

The above will gives you the values of total http requests * 2.  

_Matching Rules_

Although you can use arithmetic operators by combining two instant or range vector selectors, there are some rules that have to be followed to get
the results you want.  Essentially the labels with-in each of those vector selectors have to match.  For example, you can't do the following:

#+begin_src shell
node_cpu_seconds_total{mode="system",job="monster",cpu="0"} + node_cpu_seconds_total{mode="system",job="plex",cpu="0"}
#+end_src


[[./matching_rules.png]]

As you can see above, Prometheus returns "no data" for your query because the lebels for ~mode~ in each vector selector didn't match in both vector selectors.  
These are only the "default" rules and as you know....rules are meant to be broken!  

There exist two additional syntax keywords that you can use to tell Promtheus to "ignore" certain labels and instead operate only "on" certain lebels:

- ignoring(<label>)
- on(<label>)

Using the same vector selector and operator from above, I will now get this result:

#+begin_src shell
node_cpu_seconds_total{mode="system",job="monster",cpu="0"} + on(cpu) node_cpu_seconds_total{mode="system",job="plex",cpu="0"}

{cpu="0"}	61333.659999999996
#+end_src

_Comparision Operators_

Comparision operators are also common in IT and will look familiar:

- == - equal to
- >  - greater than
- <  - less than
- >= - greater than or equal to
- <= - less than or equal to
- != - not equal to

There also exists a ~bool~ modifier that can be used after the comparison operator.  This comes in handy because if you use the operators above 
and none are true, you will not get any results.  For example, considering the following:

#+begin_src shell
prometheus_http_requests_total == 0
#+

If there exist no time series data that equals to  zero, then you will get no results.  However, if you use the ~bool~ modifier then you 
will get results for each time series data but it's value will either be 1 or 0 (which equates to true or false).  

#+begin_src shell
prometheus_http_requests_total == bool 0
#+end_src
\\
\\
_Logical Set Binary Operators_

The operators below are used to combine sets of results and again are common in IT and programming:

- and - intersection
- or - Union
- unless - compliment

The way these operators work though is slightly different than what is expected.  These operators use labels to compare records.  For instance
~and~ returns only records where the set of labels in the first operand match the second operand.  Note the example below using psuedo queries:

#+begin_src shell
query2{A,B,C,D,E} and query1{A,B,C}
#+end_src 

The above will result in only showing results for "A,B and C".  So as you can see, these operators allow you to sort of "filter out" results on
another expresion.  Here is a summary of what each operator does:

- and    - return results only for the labels that exist in both operands
- or     - returns results for all labels from both operands
- unless - returns results only for the labels that don't exist in in both operands
\\
\\

_Aggrigation Operators_

These allow you to combine multiple values into a single value:

- sum          - add all values together
- min          - select the smallest value
- max          - select the largest value
- avg          - calculate the average of all values
- stddev       - calculate population standard deviation over all values
- stdvar       - calculate population standard variation over all values
- count        - count number of values
- count_values - count the number of values with the same value
- bottomk      - smallest number (k) of elements
- quantile     - calculate the quantile for a particular dimension

Consult Prometheus documentation [[https://prometheus.io/docs/prometheus/latest/querying/operators/][here]] for details on each (in addition to more operators) but to give you an example, you would use the ~avg~ aggrigation operation like so:

#+begin_src shell
avg(node_cpu_seconds_total{mode="idle"})
#+end_src

And of course you will get the average the cpu is idle for this machine.  
\\
\\
_Functions_

Again like Aggrigation Operators, there exist way to many functions to go over in this document but the Prometheus documentation on functions
[[https://prometheus.io/docs/prometheus/latest/querying/functions/][here]] will definitely have everything you need.  

One function of particular interest is the ~rate~ function.  It allows you to track the average per-second rate of increase in a time series value.
Remember that how many miles per hour a car is going is an example of a ~rate~.

Below are other functions that may be of use and their applications:

- abs() - calculates absolute value
- clamp_max() - returns values, but replaces them with a maximunm value if they exceed that value
- clamp_min() - returns values, but replaces them with a minimuym value if they are less than that value.  

The way functions are used is you pass arguments to them and then the function operates on those arguments.  For example:

#+begin_src shell
clamp_max(node_cpu_seconds_total, 1000)
#+end_src

The above functions takes an instant vector selector as the first argumement and a number (the max number you want to clamp to) as the second 
argument.  As you can see below, any value that is less than a 1000 will have that value, anything over 1000 has that value by default
[[./clamp_max.png]]
\\
\\
_HTTP API_

So why you would even want to use the API?  If you want to incorporate Prometheus metrics in your own tools or code/scripts, this would come in
very handy!

The end point that is made available for querying Prometheus data is @ /api/v1/query?query=<vector selector>.  This will essentially let you get 
results for a metric using curl

#+begin_src shell
curl http://prom:9090/api/v1/query --data-urlencode "query=rate(node_cpu_seconds_total{job=\"monster\",mode=\"system\"}[1h])"
#+end_src 

Because some of the query syntax conflicts with http url syntax, it requires the ~--data-urlencode~ argument to pass your actual query, in addition
to requiring some escaping of double quotes.  The end-point up above is also pretty much just for instant vector selectors.  For range vector selectors
there exists another end-point.
