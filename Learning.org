#+Title: Learning Series
#+Author: Jessie Saenz a.k.a chunky_pie
#+Date: <2024-05-27 Mon>
#+OPTIONS: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="CSS/gray.css"

[[https://www.chunkypie.me][Home]]

* *_Over-View_*

I've spent many years self-studying Linux, Scripting, Virtualization and other technologies with the hopes I can earn a living doing what I love.  I learn best by reading books, taking notes and practicing in my lab.  Below are the notes I've taken for every technology/book I've read and studied, along with the flash cards I made to help me memorize what I've learned (I've since stopped making flash cards to speed up how long it takes me to learn a technology).  I constantly refer back to my notes and they have become invaluable and my hope is they can be a resource for whoever finds them.

* *_The Notes (currently in the process of formatting and uploading all my notes)_*

[[./Learning_Bash_Cookbook/Learning_Bash_Cookbook.html][Learning The Bash Cookbook]]\\
\\
[[./Learning_Git/Learning_Git.html][Learning Git]]\\
\\
[[./Learning_Prometheus/Prometheus_Main.html][Learning Prometheus]]\\
\\
[[./Learning_Linux_Firewalls/org/Learning_Linux_Firewalls.html][Learning Linux Firewalls]]\\

