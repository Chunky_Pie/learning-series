#+Title: Deploying A Kubernetes Cluster
#+Author: Jessie Saenz
#+Date: <2022-09-01 Thu>

*_Installing Kubernetes Locally On VMs_*

NOTE:  I simply stood up 4 Ubuntu Server VM's on my network that are alread being served DNS and DHCP.  I also have the VM's on a mesh VPN overlay network that I'm using
for the kubernetes networking.  So each node plus the api server has a static IP.

_Overvew_

From what I understand, one machine is to serve as the API server and etcd host and the rest of the machines are known as the nodes.  I believe the intension is, all incoming
traffic outside of the kubernetes cluster comes in on an interface on the API Server machine and gets forwarded to another bridged interface to the kubernetes network.  In
the set-up below, the regular enp1s0 interface is where outside traffic is received and the nebula1 interface is where kubernetes network traffic goes over.

_Setting Up iptables_

The following commands need to be run on the API server *ONLY*:

#+begin_src shell
  #this sets up nat on the enp1s0 interface
  iptables -t nat -A POSTROUTING -o enp1s0 -j MASQUERADE

  #this forwards traffic received on the enp1s0 interface from already established connections to the nebula1 interface
  iptables -A FORWARD -i enp1s0 -o nebula1 -m state --state RELATED,ESTABLISHED -j ACCEPT

  #this forward traffic received on the nebula1 interface back out to the enp1s0 interface
  iptables -A FORWARD -i nebula1 -o enp1s0 -j ACCEPT

#+end_src

The following commands need to be run on *EACH* node:

This wasn't in the book but kubernetes gave an error for having swap on, on the machines:

#+begin_src shell
sudo swapoff -a
#+end_src

You also must comment out swap with-in /etc/fstab

This ensures that iptables can see bridged network traffic:
#+begin_src shell
echo "br_netfilter | sudo tee /etc/modules-load.d/k8s.conf
#+end_src

This is forwarding traffic through the bridges
#+begin_src shell
echo "net.ipv4.ip_forward=1
net.bridge.bridge-nf-call-ip6tables=1
net.bridge.bridge-nf-call-iptables=1" | sudo tee /etc/sysctl.d/k8s.conf
#+end_src
\\
\\
_Intalling A Container Runtime_

The following ensures these packages are installed.  (I set these packages to be automatically installed via cloud-init when standing up my VM's from a golden image):

#+begin_src shell
sudo apt install ca-certificates curl gnupg lsb-release
#+end_src

The following adds dockers apt repo and gpg signing key to install the latest containerd service:

#+begin_src shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu   focal stable" \
| sudo tee /etc/apt/sources.list.d/docker.list
#+end_src

Now simply update apt and install containerd.io

#+begin_src shell
sudo apt update; sudo apt install containerd.io -y
#+end_src

Configure containerd and restart the service by running the following:

#+begin_src shell
containerd config default > config.toml
sudo mv config.toml /etc/containerd/config.toml
sudo systemctl restart containerd
#+end_src
\\
\\
_Installing Kubernetes_

The following adds the encryption key for the kubernetes package from google:

#+begin_src shell
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#+end_src

Next, add the apt repo (xenial is the lastest version available from google I guess?):

#+begin_src shell
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
#+end_src

Lastly, update apt and install the kube tools:

#+begin_src shell
sudo apt update; sudo apt upgrade; sudo apt install kubelet kubeadm kubectl kubernetes-cni -y
#+end_src
\\
\\
_Setting Up The Cluster_

The following command is to be run on the API server *ONLY* and references the network used for kubernetes traffic (in my case it's the VPN overlay network):

#+begin_src shell
sudo kubeadm init --pod-network-cidr 192.168.10.0/24 --apiserver-advertise-address 192.168.10.1 --apiserver-cert-extra-sans kube-zero.chunkypie.me
#+end_src

When the above command is ran successfully (I had multiple erros initial during "flight check" that I had to resolve) you should get a command output that can be ran on
each subsequent node to join the cluster.  The command will look something like this:

#+begin_src shell
sudo kubeadm --v=5 join 192.168.10.2:6443 --token pibum5.rh7qbd26rmwuiy3s --discovery-token-ca-cert-hash \
sha256:33dd480779b405c651629fd5f53d6667f1042657542df447c271a5d62cac80cb
#+end_src

_Setting Up Cluster Networking_

So all the networking we've done thus far has been related to node-to-node traffic.  The following steps have to do with pod-to-pod networking but the book does not explain
any of the concepts.  It also doesn't state if these commands are to be run on all the nodes or just the control plane.  I ran them successfully on the control plane but was
not able to run them on any of the nodes.

#I need to just finish off this section and move onto the other chapter.  If anything wasn't done right I'll know soon.  
