#!/bin/bash

recursion () {

    for file in $(ls $thisfile)

    do

	thisfile=$thisfile/$file

#	echo "*$thisfile*"

	if [ -f $thisfile ]

	then

	    echo -e "$tab|\n$tab-----$file"

	elif [ -d $thisfile ]

	then

	    echo -e "$tab|\n$tab-----$file"

	    tab="$tab|\t"

	    recursion $thisfile

	    tab=${tab%"|\t"*}

	fi

	thisfile=${thisfile%/*}

    done

}


genisis="$@"

echo $genisis

for each in $(ls $genisis)

do

    thisfile=$genisis/$each

    tab="\t"

    if [ -f $thisfile ]

    then
	    
    echo -e "|\n-----$each"

    elif [ -d $thisfile ]

    then

	echo -e "|\n-----$each"
	
	tab="|\t"

	recursion $thisfile

#	tab=${tab%%"\t*"}

    fi

    thisfile=${thisfile%%/*}

#    tab=${tab%%"\t*"}
    
done

<<COMMENT

I got the tabs right and recursion!  Now I just have to work backwards from the most nested file/dir in the tab tree and fill in the dashes and pipe characters to simulate the "tree branches" of the file
hierarchy!  Man....this is so addicting and amazing!
COMMENT
