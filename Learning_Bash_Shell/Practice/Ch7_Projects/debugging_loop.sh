#!/bin/bash

<<NOTES
So I am to create a script that emulates the behavior of a C style for loop:

   i.e. Initializing the iterating variable, condition for iterating variable, incrementing iterating variable

So I may have to provide for three arguments to the variable: name/value, conditions and increment.  What about the operations to be executed while condition is true, how would I implement that?

Lets start with the easy stuff at least.  Build the script to take in the argument of variable name and value.

So I started over and re-thought my approach.  I'm anticipating the user providing arguments forthis c-style for loop in a format similar to: "i=0 i -lt 10 i++"

My thoughts we're to break the arguments into pieces and have those pieces delimited by whatever is to the left of "=" which the user is likely to provide, considering you need to initialize a variable in a for loop.  From there, I'll know what the variable name would be plus the condition and incremental value.  The code below sets the delimter to whatever is before a "=" in the arguments to the script and fills in those pieces into an array and takes out any null values.

I want to try to include the IFS initialization into the while statement.  Maybe like:

  while IFS="${1%=*}" [ <condition ]

  	or

  while IFS="${1%=*}" && [ <condition ]

12/31
I realized I didn't even have to do a loop and just setting the delimiter to whatever the user specifies as their variable and then feeding that line into an array worked just the same.

I still couldn't figure out why the first index in the array was null, a white space or just a newline.  I ended up just unsetting that in the script.
NOTES

#Variables and loop to break the arguments into workable pieces: iterator variable, iterator condition and iterator incremental - provided by the user
line="$@"

IFS="${1%=*}"

args=($line)

unset args[0]
