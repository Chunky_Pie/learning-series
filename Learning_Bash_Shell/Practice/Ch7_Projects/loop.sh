#!/bin/bash

<<NOTES
So I am to create a script that emulates the behavior of a C style for loop:

   i.e. Initializing the iterating variable, condition for iterating variable, incrementing iterating variable

So I may have to provide for three arguments to the variable: name/value, conditions and increment.  What about the operations to be executed while condition is true, how would I implement that?

Lets start with the easy stuff at least.  Build the script to take in the argument of variable name and value.

So I started over and re-thought my approach.  I'm anticipating the user providing arguments forthis c-style for loop in a format similar to: "i=0 i -lt 10 i++"

My thoughts we're to break the arguments into pieces and have those pieces delimited by whatever is to the left of "=" which the user is likely to provide, considering you need to initialize a variable in a for loop.  From there, I'll know what the variable name would be plus the condition and incremental value.  The code below sets the delimter to whatever is before a "=" in the arguments to the script and fills in those pieces into an array and takes out any null values.

I want to try to include the IFS initialization into the while statement.  Maybe like:

  while IFS="${1%=*}" [ <condition ]

  	or

  while IFS="${1%=*}" && [ <condition ]

12/31
I realized I didn't even have to do a loop and just setting the delimiter to whatever the user specifies as their variable and then feeding that line into an array worked just the same.

I still couldn't figure out why the first index in the array was null, a white space or just a newline.  I ended up just unsetting that in the script.

1/2
I created a simple for loop so I can see the content of each array  index and noticed that because I unset index 0 in the code, the first word in argument to the script wasn't there.  When I removed the unset 0 index line, that word from the argument appeared but as before, with an empty new line at the beginning.  So it may be that there is a newline on that index 0 and not just comletely empty.  For next try, attempt to add a trim to each index so it cuts out any newline.

1/3
So the above doesn't work.  Every time I try to unset the index that I believe carries a null value, it still remains.  I keep thinking if I trim an empty space from $line, it will help but I forget that $line is the entire line from the command line.  I need to figure out how to initialize the array as long as there is no space in the value.

1/3 (evening)
What I tried was using the indexes of args array (trimming a single space from the end of the string) to initialize a args2 array which worked to eliminate an empty string at the first index but then there existed empty index at the end of the array!  Geeze!  Well at least I realized I was attacking the problem from the wrong direction trying to manipulate the $line string and instead need to figure out what is in those empty indexes?  I ran -n and " " conditions but none worked when evaluating the presumably empty index?

NOTES



#Variables and loop to break the arguments into workable pieces: iterator variable, iterator condition and iterator incremental - provided by the user
line="$@"

IFS="${1%%=*}"

args=($line)

echo "#########################For Each Loop########################"
counter=0

for each in "${args[@]}"

do

 #   echo -e "This value is being checked: ${args[$counter]}"

    if [ -n "${args[$counter]}" ]

    then
	
	args2+=("${args[$counter]# }")

	echo -e "${args2[$counter]}"
	
    fi

    ((counter++))
    
done

echo -e "\$args2 array is: ${args2[@]}"
echo "#########################For Each Loop########################"

#for ((i=0; i<="${#args3[@]}"; i++))

#do

 #   echo -e "${args3[$i]}"

#done

#unset args3[3]

#echo -e \\n"Total indexes in array \$args3 is: ${#args[@]}"

test=" string "

echo -e \\n"${test##*}"buffer

echo -e \\n"${test%%${test## string} }"buffer

#test=${test%%${test## *} }

#echo -e "$test""buffer"
