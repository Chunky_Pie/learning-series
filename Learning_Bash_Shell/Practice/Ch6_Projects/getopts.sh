#!/bin/bash

<<COMMENT
Options
-e is for echo options and will have an argument for an echo option
-v is going to have an argument that will be what echo "echos"

Then <arg> will be the argument I give to the script
COMMENT

<<NEXT_COMMENT
getopts ":e:" opt

echo -e "The first option is $opt and the argument to that option is $OPTARG"

getopts ":v:" opt

echo -e "The second option is $opt and the argument that option is $OPTARG"

shift $(($OPTIND-1))

echo "This is positional parameter 1: $1"
NEXT_COMMENT

while getopts ":e:v:" opt; do

    echo -e "This is option $opt and the argument to that option is $OPTARG"

done

shift $(($OPTIND-1))

echo "This is positional parameter 1: $1"
