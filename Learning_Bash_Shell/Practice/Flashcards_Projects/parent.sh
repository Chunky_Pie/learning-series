#!/bin/bash

#set -o verbose

#set -o xtrace

create() {

    for ((i=1; $i <= '11'; i++)); do

	PS4="Create Function for parent$i: "

	mkdir ./parent${i}

	cd ./parent${i}

	echo "echo ${export:-not exported}" > parent.sh

	chmod +x parent.sh

	./parent.sh

    done
    
}

echo ${export:-not exported}

create
