#!/bin/bash

PS4='DEBUG Line: $LINENO - '

set -o xtrace

returnfunc() {

    if [ "$FALSE" != "$TRUE" ]; then

	echo -e \\n"This should return 0 foo!"

	return 0

    else

	builtin echo -e \\n"This should return 1"
	
	return 1
       
    fi
    
}

returnfunc
