#!/bin/bash

star="$*"

echo -en "For loop is using \$* as a list: "\\n

for each in "$star"; do

    echo -e "$each"\\n

done


pound="$@"

echo -en "For loop is using \$@ as a list: "\\n

for each in $pound; do

    echo -e "$each"\\n

done
