#!/bin/bash

string1='cat'

string2='boat'

if [ "$string1" \> "$string2" ]; then

    echo -e \\n"$string1 is greater than $string2"

else

    echo -e \\n"$string1 is not greater than $string2"

fi
