#!/bin/bash

_declare() {

    var="variable"

    echo -e "This is from with-in the function: $var"

}

_declare

echo -e "This variable \$var is declared from with-in the function using the declare builtin: $var"
