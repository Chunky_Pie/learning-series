#!/bin/bash

afunc1() {

    echo -e \\n"$1:  Came from afunc1"

}

#function 2
afunc2() {

    echo -e \\n"$1:  Came from afunc2"

}

#function3
afunc3() {

    echo -e \\n"$1:  Came from afunc3"

}
#trap
trap 'echo -e \\n"\tDis my trap game foo!"' RETURN

declare -ft afunc1 afunc2 afunc3

afunc1 "$@"

sleep 3s

afunc2 "$@"

sleep 3s

afunc3 "$@"
