.
If/Else
_______

One thing to keep in mind regarding If/Else statements in bash is the requirement of exit statuses and returns.  The condition that is being evaluated by an if statement is basically the exit status of a command you use as the conditional.

    i.e. - if <command>

    	   then

	        Statement
		Statement
		Statement

	   else

		Statement

	  fi

else if
_______

     i.e. - if <condition>

     	    then

	         Statement
		 Statement
		 Statement

	   elif <condition>

	   then

	        Statement
		Statement
		Statement

	   fi

Exit Status 
___________
Every command returns what is called an integer code to it's calling process.  In our case, bash is the calling process (i.e. bash executes a command, when that command finishes it returns an integer code to bash.)

Conditions used in flow control constructs (i.e. if/else) are basically just commands that are run and if the command executes successfully then that command returns an integer code, if the command does not execute successfully then the command returns a differen't integer code.  That "integer code" is refered to an "exit status."

If the command runs successfully then the condition evaluates to an exit status of zero and if it doesn't run successfully it evaluates to an exit status of anything between 1 and 255.

Summary:

	0 = true
	1-255 = false

Note:  The above summary is convention and not law.  The diff command in particular gives an exit status of 0 if no differences are found, a 1 if differences are found and a 2 if argument is invalid.


Built In Commands
_________________
If you name a function with the same name as a Unix command and use that Unix command inside of that function, you need to call the command using the "builtin" command which tells the shell to use Unix command and ignore the function with the same name.  This is because the Shell gives priority to functions in it's order of command look-up.



Return Statement
______________
With-in functions, the exit status that is returned by the function is the one belonging to the last command.  Using the return statement "Return N" returns an exit status of N.  If N isn't used then it defaults to the last command like mentioned above.  

     i.e. - func() {

     	         cd /home/jsaenz

		 echo $PWD

		 what

	    }

The above function calls two commands that run succesfully and the last line will error out as there is no command called "what" and as such will cause the function to return an exit status of 1 despite "what" being a simple typo.  To avoid this, you can use the special variable ? which is assigned the value of the last commands exit status.  Simply assign another variable the exit status of a command of your choice (this is done after that command is ran) and then use the "return" keyword at the end of the function to return that exit status.

     i.e. - func() {

     	         cd /home/jsaenz

		 es=$?

		 echo $PWD

		 what

		 return $es

            }

Note:  Return keyword can only be used with-in functions or with-in scripts that are called with source.  In contrast the exit keyword exits the entire script, no matter how deeply nested you are in a function.



Combination of Exit Status
__________________________

You can use "and" and "or" operators to further extend conditional statements. This works by evaluating the exit status of a command and depending on the operator you choose, depends on what happens next.

&& is the "and" operator

|| is the "or" operator

By using the && operator, you're saying if the exit status of the first command is zero (successful) then proceed to executing the next command.

By using the || operator, you're saying if the exit status of the first command is one (unsuccessful) then proceed to executing the next command.  

     i.e. - if cd /home && cd /home/jsaenz

     	    then

		statement
		statement

 	    fi

	    if cd /home && cd /home/jsaenz

     	    then

		statement
		statement

 	    fi



Conditional Tests
_________________
Without using conditional tests like I'm going to explain below, "if" constructs are really just examining the exit status of a command.  You can test more than the exit status of commands.  You can actually test other items by either using the "test" keyword (this is actually a builtin command btw) or a condition with-in brackets.

NOTE: when using variables with-in [ ] constructs, always QUOTE them.  You will run into problems if those variables evaluabe to null expressions without being quoted.

    i.e. if [ <this is true> ]

    	    then

		do this

	fi


	if test <if this is true>

	   then

		do this

	fi



String Comparisons
__________________

With-in a conditional test you can use string comparison operators to check the length of the string.

=

!=

< (this applies lexicographically to the order of the alaphabet)

> (same as <)

-n (means the string is not null

-z (means the string IS null)

When using string comparisons with-in a conditional test, it's important to enclose your string with-in double quotes, especially if it's a variable.  Bash will expand the variable and if it contains more than one string you will receive an error because the -n unary operator operates on only one string, not multiple.  Using double quotes treats the item as one string.  Also, another lesson I had to learn the hard way was that you have to escape the < or > sign for it to apply correctly.  Otherwise, use the updated [[ test command to omit escaping the < and > operators.  Geeze this took me two hours to figure out!  But I figured it out!

Another item to note with alphabetical order is to understand that the order is evaluated from "a" being the least (the 1st letter in the alphabet) and "z" being the greatest (the 26th letter in the alphabet).  Also, capital letters are great than lower case letters.  (A is greater than z)

Last item to note is when using string comparison operators, there must be a spece between the operator and the operands (i.e. [ string1 = string2 ], not [ string1=string2 ])



File Attribute Operators
________________________

-d - File exists and is a directory
-e - File exists

-f - File exists and is a regular file (i.e. not a directory or other special type of file)

-r - You have read permission on file

-s - File existsand is not empty

-w - You have write permission on file

-x - You have execute permission file or directory search permission if it is a directory

-O - You own file

-G - Files group ID matches yours

-nt - Newer than operator

-ot - Operator

Remember, you can use the logical operators "&&" or "||" in combination of commands and conditional tests or betwen two conditional tests like so:

	  if [ condition ] || <shell command>; then

	  if [ condition ] && [ condition ]
	  


Logical Operators With-in Test Commands
_______________________________________

-a - is the and operator

-o - is the or operator

The above operators work inside of [...] test commands.  Groupings of this sort need to be with-in parenthesis and those parenthesis have to be escaped:

    if [ $amount -lt 5 ] && [ \(-n $string\) -a \(${#string} -gt 10\) ]

It makes sense to say that when using && or -a, it's redudant to even use -a and to just create a seperate test command for each condition we want to evaluate.  I can only think of this becoming handy if we're using or logic; where you would want a complicated expression using -a or -o to be evaluated AND/OR another test command to be evaluated:

   if [ $amount -lt 5 ] || [ \(-n $string\) -a \(${#string) -gt 10\) ]

Maybe my logic is right here but I dont think you could evaluate the above without using the logical operators with-in the test

   if [ $amount -lt 5 ] || [ -n $string ] && [${#string) -gt 10 ]

Note: The shell takes precedence in the order it processes operators.  Also, when using escaped parenthesis to group conditionals that include arithmetic expressions, it's important to note there must be a space after the parenthesis. 

Integer Conditionals
____________________

-lt - Less than

-le - Less than or equal to

-eq - Equal to

-ge - Greater than or equal to

-gt - Greather than

-ne - Not equal to



For Loops
_________

Syntax:

for <name> in [list]

do

	satements that can use $name....

done

Note:  You can omit the [in list] and it will default to $@ (the list of quoted command line arguments)

Also, remember the IFS variable you can use to initialize delimiters.



C Style For Loops
_________________

This one I got from researching the internet

for ((<iterator>;<condition;<increment>))

do

	Statements to iterate

done



Case
________

Syntax:

case <expression> in

     <pattern1> )

     		statements;;

     <pattern1> )

     		statements;;

     ...

esac

Note:  Case statements really are just simple ways to test variables.  You can't do anything like in if statements using [ -f <insert here]



Select
________

Syntax:

select <variable> [in list]

do
	statements

done

Select works just like a for loop except it presents the user with the "list."  You must insert a break after your last statement to ensure the loop finishes.  Otherwise it will just continue to loop and loop, continually asking the user to make a selection.

The $PS3 prompt string variable is the default variable for the title of the users selection.  See below:

select selection in $(ls)

do

     echo " this is your selection"

done

What running the above select construct looks like:

1) file 1
2) file 2
3) file 4
#? <----this is what the $PS3 variable displays by default.  If you initialize the variable to something different, that will show instead.



While and Until
_______________

Syntax:

while <condition>

do

     statements

done

until <condition>

do

     statements

done

The while construct continues it's loop while the condition is true and the until construct continues it's loop until the condition is true.

So basically the "until" loop allows you to run a loop while something is "false" and the "while" loop allows you to run a loop while something is "true."

The book states that you can use the ! operator to accomplish the same thing as an "until" loop, using the "while" construct.
    i.e. while ! <some item that's true>

    	 do

		<some statements>

	done

You can use the sleep command to specify an amount of time for a program to delay
    i.e. while ! cp $1 $2

    	 do

		echo "Whats up foo, you don't have permission!"

		sleep 5

	done

The above example echo's a statement then sleeps for 5 sec's and continues through the loop while copying argument $1 to argument $2 cannot be done.