Process ID and Job Numbers
__________________________

So there is a difference between Process ID's and Job Numbers.  Process ID's are assigned to process running on the entire system for all users by the operating system.  While job numbers are assigned to commands ran in the background by your shell .


Foreground and Background
_________________________

You can use the builtin command 'fg' to bring a background job back to the fore ground.  If you have only one job running in the background, running 'fg' without arguments will bring that single job back to the fore ground.

If you have more than one job running in the background then you want to provide an argument which defines the job you want to bring to the fore ground:

   i.e. fg %<command name> | <job number>

   	fg %./script

	fg %2

You can use the 'jobs' command to list the currently running jobs.  Below are the available options to 'jobs.'

-l Also lists process ids
-p Lists ONLY process ids
-n Lists only those jobs whos status has changed since the shell last reported it
-r Restricts the list to jobs that are running
-s Restricts the list to those jobs which are stopped. i.e. - Waiting for user input
-x Executes a command


Suspending A Job
________________

If you've brought a job to the fore ground, you can suspend a job by pressing ctrl Z.  This 'pauses' the job and gives you control back of the shell.  Although you have the shell back, your process is paused.  If you want to continue running that job in the background then press bg after you've pressed ctrl Z to suspend the job.

Some good uses of ctrl Z is while in the terminal editing a file, you can 'suspend' that editor to regain control of the terminal (lets say to process that file you're editing) and then bring the editor back to the file you we're editing by executing 'fg.'  Another use of the above may be when you execute a command in the terminal but it's obviously taking longer than expected.  You can 'suspend' it with ctrl Z and then move it to the background to continue running while you work in the terminal using 'bg'.


Signals
_______

Signals are built into the operating system as a way for processes to talk to other processes.  This can be because something has gone wrong or a process wants another process to do something.  Typically a process wants to affect a subprocess it created.  As I've learned in previous chapters, pipes is another method from which a process can communicate with another process.  Signals and Pipes are methods of communications called "Interprocess Communication" or IPC.

There are dozens of signals defined with-in Linux and they can be called by their number or name.  To get a list of all the signals that exist you can use the -l option to the kill command like so:

      kill -l


Control-Key Signals
___________________

I'm already familiar with ^char key sequences.  ^D stops the process from reciving input, ^C I used to think stops a process, which now by reading I understand means to "interupt" or INT (but actually does stop the process).

To summarize the following control key sequences translate to these signals:

   ^C - INT or interupt
   ^D - eof or end of file
   ^Z - TSTP or Terminal Stop
   ^\ - Quit (a stronger signal to send when ^C doesn't work)

You can customize the mappings of these control key sequences to execute differen't signals by using the stty command.

    stty <signalname> <control character>

    stty erase ^I

You can obtain a list of all the signal names available to customize by using the -a option to stty.

    stty -a


Kill
____

The kill command allows you to send signals to any process.  By default, the kill command sends the terminate signal to whatever program you give it an argument as.  You can reference that program by it's pid, job# or command name.

If you want to send a differen't signal to a process you simply add the name of the signal or it's number, as an option to the kill command preceeded by a dash.  

   kill -<signame or number> <process>

   kill -9 bash

What a program does when it receives a signal is determined by the code it was programmed with.  Some programs are written to "trap" a signal they receive and behave differently than what the user intends from sending the signal.

For example, a text editor is usually programmed to "trap" a term signal and execute something else before it process the signal (usually like saving your work) and terminating.  The only caveat is the kill signal itself (kill -KILL <process>) which no program can "trap" the OS is strictly instructed to immediately kill any program when given the kill or 9 signal.

A handy options to kill is -l which allows you to list all signals or translate a name to number or vice versa.

  kill -l

  kill -l 9

  kill -l kill

It's also important to know that using ^C or ^\ to term or quit a process is better than trying to kill or 9 program as term and quit allow a process to "clean up" before stopping where as explained above, kill or 9 just immediately stop a process dead in it's tracks!  Lastly, after stopping or killing a process, a "core" file may exist


ps
__

ps is a command that allows you to see what processes are running on your system.  Without any arguments, ps will show you the processes currently running and launched by the shell you invoked ps from.  The information about those processes are as follows:

PID   TTY	TIME CMD

PID, stands for processes ID.  TTY stands for the psuedo terminal that process was launched in.  Time stands for the amount of processor time that process has used and CMD is the command name.

Because ps without any arguments is limited to the current shell only, it doesn't help you find elusive PID's or those started by other shells.  This is where ps -a comes in handy.  This argument gives you the PID's started by any sub-shells by any user.  However it will not show you the PID of a parent shell.  i.e. - The shell that started a process.  Again, with this output you'll see the PID, TTY, TIME and CMD for all process started by any user via any sub-shell.

Again, this might not be sufficient when looking for a PID that is the ever more elusive.  Because sometimes a process might have "forgotten" what parent shell or window started it, we have the -x or -e argument (-e is for system v and -x is for BSD derived systems) which shows all processes running on a system, regardless of who or what started them (user or parent shell/window or non-shell/window)


Trap
____

So the trap command is a built in that allows you to "catch" a signal and execute a command before that signal is processed or in leiu of executing that signal.  (I believe the "execute a command before that signal is processed" only applies if the signal argument is exit or 0)

You have to put your trap command before the code you want to set a trap for, with-in your script/function.  You can set multiple traps this way as the trap command won't apply to any code above it.

    trap <command> signal


Traps And Functions
___________________

This section's purpose is simply to note that if you define a trap before a function call and that function itself includes another trap, the trap with-in the function will execute instead of the one before the function call.  (Hope this makes sense foo!)


Process ID Variables and Temporary Files
________________________________________

Remember there exist special built in variables like $ - which contains the value of the current shell ID

! - contains the value that is the process ID of the most recently invoked background job

  $$

  $!


Ignoring Signals
________________

There may be times when you want to ignore a particular signaled received by command/script/function.  You can use trap for this and basically give it a null string to execute when that signal is received, like so:

trap "" <signal>

<statement/function>

A particular signal that you'll likely want to ignore is the hangup signal - HUP.  There actually exists a command that allows you to prevent a script/command/function from terminating due to a HUP signal: nohup.

  nohup <command/script/function>

Because this command is designed to redirect STDOUT to a nohup.out file in your home directory and STDERR to STDOUT by default.  It may be wise to redirect output to a file of your choice, like so:

  nohup <command> > file 2&>1


disown
______

disown is a builtin that allows you to tell bash to "forget" or "disown" a particular job.  From then on, bash will have no record of that job and therefore can't do anything to it (like send it a signal.)  It does this by removing that job from bash's "job table."

disown takes as an argument the PID or job# and has a special option, -h, which performs the same function as nohup.  You must however, specify for yourself where you want the output from that process to go.

Self Talk:  Does that mean the default ouput from nohup, is actually redirecting output from the process nohup is affecting and not from nohup itself?  I guess it makes.


Resetting Traps
_______________

After you've set a trap for a particular signal with-in your code, you may want to reset the normal behavior for that signal so that it applies to the rest of your code.  You can do that by supplying a "dash" as an argument to trap for that particular signal.

  trap - <signal>


Coroutines
__________

Coroutines are processes that are running at the same time.  In a script, if you invoke command/function/script to run in the background and while that is running, execute another command/function/script, they run together simultaneously and if the last comm/function/script finishes before the others, that can cause problems.

There exists a builtin called "wait" which will cause the shell to wait for all jobs in the background to finish before exiting itself.  "Wait" without arguments waits for all jobs where if you provide a PID as an argument, "wait" will wait until that specific job finishes.

According to the book, "wait" byitself is sufficient enough for use in any scripts.


Advantages and Disadvantages of Coroutines
__________________________________________

First some new concepts I learned that will help explain the heading of this section.  The workload of process can be categorized by how it uses system resources:  CPU intensive, I/O intensive or Interactive.  Also, the term "Thrashing" describes the phenomenon of a computer being almost halted to a stand still because of the excessive switching of CPU time between two or more process with simliar workloads (this context switching is called time-slicing.)

So....if you start two processes with similiar workloads at the same time (coroutines) you risk the above performance liability.  A wiser choice would be to inteligently combine processes with differen't workloads to take advantage of accomplishing tasks simultaneously.


Parallelization
_______________

This topic basically is a word used to describe what running two or more processes in a coroutine does.  It allows the parallelization or a task which can be very advantageous on a multi-CPU or in todays age, multi-core/threaded CPU computers.  Although you don't have to worry about thrashing as much (I would say of a factor equal to how many cores your CPU has) what does add to the complexity is the amount simultaneous of moving parts concurrently accessing the same system resource.  This problem is known as concurrency control issues.


Coroutine and Parallelization Review
__________________________________

Coroutines are when two or more processes are runnning at the same time.  You accomplish this by executing a command in the background plus each additional command in the background and ensuring you use a wait command to provide enough time for each command to finish executing before concluding the function/script.

Carrying out the above is described as parallelization or "parallelizing" your script.  Although there are execution speed benefits of this, it also comes with the burden of ensuring you don't run into concurrency control issues.


Subshell Inheritance
____________________

An important thing to note was the attributes a subshell inherits from it's parent shell and what is it's own:

Inheritance:

	The current directory
	Enironment variables
	STDIN, STDOUT, STDERR plus any other open file descriptors
	Signals that are ignored

Unique to it:

       Variables
       Handling of signals that are not ignored.



Nested Subshells
________________

Running a process in the background causes that process to be executed by a subshell and in parralel to the next process.  Running a process with-in parenthesis also causes that process to be executed by a subshell but it's not ran in the background and the shell won't go to the next command until that first process is finished.  However, running a process in the background and with-in parenthesis still only spawns 1 subshell.

So to review, remember that code in a script is executed line by line.  i.e. - If you call a function, the next line of code in the script will not be read/executed until that function finishes process.

To increase the speed of your script, you can utilize running different commands/functions in the background.  What benefit is gained from running a process in another subshell but not in the background, I don't know.  Maybe the book will explain why you would want to use parenthesis only and not &.

There are differences in inheritance between subshells spawned using parenthesis and those using curly braces.  Script blocks with-in curly braces inherit everything from their parent shell, including variables and traps.  Whereas with parenthesis subshells, those are unique to it and to it only.

According to the book, this is an important attribute of programming languages that support 
ode nesting as they give you more control?  However, the book also states that parenthesis subshells aren't as efficient as code blocks?  Gone test this.  While testing I find that not only cannot I not use script blocks with-in functions to call functions but even with-in the script, I can't use the script block to call functions with-in the script.  This even applies to just commands, you can't even call commands with-in a script block, inside of a function.  Something with how bash is interpreting the curly braces that exist with-in the function and the script block.

Notes:  So....I have to test the paragraph above but, while creating my flashcards I learned these important distinctions about subshells:

When bash executes a script, that script is ran inside of a subshell
     Any builtin commands that subshell runs, is ran inside that same subshell
     	 Any external commands, code with-in parenthesis or code ran in the background are ran
	 with-in another subshell
     Any code ran with-in a script block is ran in the same shell


Process Substitution
____________________

Process substitution is the concept of substituing the STDIN or STDOUT of a command with the data from a process.  For example, if you compare two files using the cmp command you would give it the names of two files to compare as input.  You can subsistute that input for the output of multiple processes/commands.  Vice versa is also true, you substitue the output of a command to multiple processes/commands.

The feature of process substitution is processing that input/output concurrently to multiple programs.  i.e. You can send the output of multiple programs to a process via substitution or you can send the output of one process as the input to multiple programs.  That's the kicker!

syntax

    cmp <(process1) <(process2

The above redirects the output of two programs as input to cmp

    cat /etc/passwd <(process1) >(process2)

The benefit of the latter is you can concurently use multiple programs to process output.  i.e. you have output from a packet capture and you have on program process that output for one purpose, you have another program process that output for another purpose, etc, etc.

Summarization:

The <() or >() construct basically creates a file (temp file/stream of data with a number to describe the file) and fills that file with data.  That data is either created by the program ran with-in the <>() construct and redirected to another command or data is redirected to the command with-in the <>() construct and used as input to the command

Here are links that describes more subtlties on process substitution:
     https://stackoverflow.com/questions/51293767/what-is-the-difference-between-using-process-substitution-vs-a-pipe
     https://linuxhandbook.com/bash-process-substitution/

