#+title: Working With Dates And Times
#+author: Jessie Saenz
#+date: Auguest 13th, 2021
#+OPTIONS: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../CSS/gray.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../CSS/gray.css"/>

[[./9_Finding_Files_find_locate_slocate.html][Previous]]
[[./12_End_User_Tasks_As_Shell_Scripts.html][Next]]
[[./Learning_Bash_Cookbook.html][Home]]

* _Date Comand_

strftime - format codes that are used to format a date string

+ - An option to the date commmand that allows you to specify how you want the date formatted.  If you surround the + sign and the format 
    specifier in double quotes, any non-strftime specifier will be printed as a literal string.

#+begin_src shell
date +%D
#+end_src

or

#+begin_src shell
date "+This is in MM/DD/YY format %D"
#+end_src

ISO 8601 - The recommended standard for displaying dates and times.  It is a recognized standard, there's no way to misunderstand it, it makes it easy to parse and sort.
           i.e. - Januray 5, 2021 is 2021/1/5.  So it's year, month and then day.

-d -This option allows you to specify a string as a date relative to the current date.  You can even use a date relative to other dates! i.e. - 5 days from today, last thursday or
    last thursday +7 days.  Use man getdate to learn more about the power of the -d option.

#+begin_src shell
#print out the date of 7 days past last thursday!

date -d "last thursday +7 days"
#+end_src

Epoch seconds - This is how many seconds have passed since the Unix Epoch.  This event is designated as Janurary 1, 1970 UTC.  Every *nix system keeps a running cound of how
                many seconds have passed since that date.  You can easily do date difference math by converting a date to epoch seconds.


#+begin_src shell
#code to convert epoc seconds to a human readable date
epoc='1970-1-1 UTC'

current_epoc_seconds=$(date +%s)

date -d "$epoc $current_epoc_seconds seconds" +%D
#+end_src

There is also a syntax you can use that is shorthand for converting a epoc seconds date to human readable form
#+begin_src shell
current_epoc_seconds=$(date +%s)

date -d "@$current_epoc_seconds" +%D
#+end_src

#+begin_src shell
#convert some other date to epoch seconds
date -d '<date>' +%s
#+end_src

You can use the date string to get the date for a time span of period.  i.e. - What date was 5 days from last christmas would be:

#+begin_src shell
date -d "2021-12-25 5 days"

Thu Dec 30 12:00:00 AM MST 2021
#+end_src

Or, you can add a minus sign to give you a date of 5 days before last christmas:

#+begin_src shell
date -d "2021-12-25 -5 days"

Mon Dec 20 12:00:00 AM MST 2021
#+end_src 
