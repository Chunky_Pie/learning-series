#+title: Writing Secure Shell Scripts
#+author: Jessie Saenz
#+date: 12/2/21

* _Security Template_

  Below are important variables and items to set in your bash scripts to keep them more secure than if they were omitted.

  #Set a sane/secure path and can be gleamed from your current .bashrc PATH variable if it's paths are known and intentinal at the moment
  PATH='usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/jsaenz/.cargo/bin:/home/jsaenz/.local/scripts/bashdb'
  
  #Export path incase it's not already done so (we alias escape all commands in case anything funny is happening with an unknown alias to a common command)
  \export PATH

  #Clear all aliases
  \unalias -a

  #forget all remembered command path locations from bashes command hash table
  hash -r

  #Set a hard limit to the resources available to bash, specifically the maximum size of core files created (essentially turning off core dumps)
  ulimit -H -c 0 --

  #Set a secure and sane value to IFS env variable
  IFS=$' \t\n'

  #Set a secure and sane umask settings
  #NOTE:  This does not affect files already redirected on the command line
  umask 7027

  #Create a unique random directory
  until [ \( -n "$temp_dir" \) -a ! \( -d "$temp_dir" \) ]; do

      temp_dir="/tmp/temp.${RANDOM}${RANDOM}${RANDOM}"

  done

  mkdir -p -m 0700 $temp_dir || \

  (echo "FATAL: Failed to create temporary directory '$temp_dir': $?"; exit 100)

  #Do our best to clean up temp files no matter what
  #NOTE:  $temp_dir must be set before this and must not change!
  function cleanup {

      rm -rf $temp_dir

  }
      
  trap cleanup ABRT EXIT HUP INT QUIT

  
* _Interpreter Spoofing_
  
  One of the concepts here revolves around what is called the "interpreter directive", "magic line" or "shebang."  It's the very first line in a script
  that the OS reads so it knows what program to use to run your command (in this case our script.)

  That interpreter directive is the path to that program and once found, it feeds your command/script as an argument to that program using the
  full path that it found.

  i.e. - If you execute a script called "execute" and the program that is going to run your script is located at path /usr/bin/bash then bash build the
         subsequent command to run as "/usr/bin/bash execute."

  The second concept surrounding "interpreter spoofing" is regarding setuid's and sguid's which allow one to run a command as the owner or group owner
  of that file.  Manipulate these two concepts and you have interpreter spoofing.

  If a threat actor finds a file with the suid or sguid bit set, it can make a hard link copy of that file to a directory it has access to and give
  it that same name as an option that exists for that program that will run your script.  Then when it calls the hard linked script by it's new name,
  which is that "option", the shell will build the command as "/usr/bin/bash option".  i.e. /usr/bin/bash -i (which pops an interactive shell)

  #+begin_src shell
    ln -- execute -i
    -i
  #+end_src

  NOTE:  For the above to work, you also have to add the current directory as the first path to your PATH environment file.  This is so bash will
         look for a command or script to run named -i, in your current directory where that script exists.

       
  You protect against this by including a - or -- to your interpreter directive

  #+begin_src shell
    #!/bin/bash -
  #+end_src


* _Clearing All Aliases

  unalias as you know removes an alias definition but the -a option removes all aliases from the environment.  Whats important to note is to
  preceed the command with a '\' to prevent alias lookup of that command.


* _Clear The Command Hash_

  The hash command allows you maninuplate bash's command location dictionary and the -r option forces bash to remove all indexes from that array


* _Preventing Core Dumps_

  ulimit - This is a command that allows you to set limits on resources available to the shell and processes

  -h - Option to set a hard limit for a particular resource

  -c - Option that specifies the maximum size of core files created

  #+begin_src shell
    ulimit -h -c 0
  #+end_src

  The above command sets a hard limit to the amount of core dump files that can be created by the shell


* _Setting A Secure $IFS Variable_

  It is desired to set IFS to the expected standard space, tab, newline and prevent that variable to be set to something malicious ot unexpected.
  What it could be set to that would cause something malicious is beyond my feeble understanding of bash.  I'm wack at this point (hopefully will
  get better.) : /

  #+begin_src shell
    IFS=$' \t\n'
  #+end_src


* _Using Secure Temporary Files_

  Creating  a unique random directory helps ensure the permissions to those temp files are secure (not readable/writable to everyone.)  Plus it
  obsures the name of it so it isn't predictable.  One can use the mktemp command to ensure the two requirements of secure temp files.

  $RANDOM - Shell variable set by the shell that gives a random number between 1 and 32767.


* _Writing Suid or Sguid Scripts_

  It's not good practice to set a script with the suid or sguid bit set, especially for scripts owned by root.  Another security implication of this
  is when it applies to a directory, all files created with-in this directory to be owned by the directory's owner or group.


* _Restricting Guest Users_

  So restricting a user using the methods below only apply to the shell used to log into the system, not the 
