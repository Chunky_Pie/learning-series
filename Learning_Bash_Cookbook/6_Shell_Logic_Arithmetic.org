#+title:  Shell Logic And Arithmetic
#+author: Jessie Saenz a.k.a chunky_pie
#+date: <2024-06-04 Tue>
#+OPTIONS: num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../CSS/gray.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../CSS/gray.css"/>

[[./7_Intermediate_Shell_Tools_I.html][Next]]
[[./Learning_Bash_Cookbook.html][Home]]

* _Doing Arithmetic In Your Shell Script_

=**= - Allows you to raise to a power as in $((2**8))

Remember that with-in the $(( )) construct, you don't have to preceed variable with $ unless it's a positional parameter, then you do.

#+begin_src shell
$((var+var2))
#+end_src

or

#+begin_src shell
$(($1+$2))
#+end_src


* _Testing With Pattern Matches_


You can test patterns but only with-in the [[ test construct.  You must still respect the white space requirement of bash after the opening [[ and clossing brackets and you must not quote the string
pattern for it to be matched as a pattern but you still need to quote the variable incase it is null or empty.  Lastly, you can use single or double equal signs. i.e. - 

#+begin_src shell
if [[ "$var" == *ttf ]]
#+end_src

The above will match any string that ends with 'ttf'

There exist more pattern matching syntax to use with-in [[ test constructs which allow you to match several patterns separated by | operator but they can only be accessed by turning on the extglob option with-in bash using shopt, which I believe is on by default.  These
additional pattern matching symbols are as follows:

@(...) - Match only one occurance of what is in parenthesis

*(...) - Match zero or more occurances of what is in parenthesis

+(...) - Match one or more occurances of what is in parenthesis

?(...) - Match zero or one occurance of what is in parenthesis

!(...) - Negate a match

Example:
#+begin_src shell
var='foobar'

if [[ "$var" = *(foo|bar) ]]; then

    echo "A match was found!"

fi
#+end_src

It's important to note that what is matched with-in the pattern is case sensitive however you can activate the nocasematch option using shopt.  

NOTE:  You can't have any spaces before or after the '|' operator when using pattern matching but you can use globstars

* _Testing With Regular Expressions_

You can test using regular expressions with-in the [[ construct by using the =~ operator.  When using regex you can take advantage of character classes like:

[:alpha:] -  Match any range of alphabetic characters (upper or lower case.)

[:digit:] - Match any range of numbers

[:alnum:] - Match aphanumeric characters

[:blank:] - Matche any range of spaces or tabs

An example of how we would use the character classes to match a regex expression is as follows:

#+begin_src shell
if [[ "$string" =~ [[:alpha:]+[:digit:]*]@[:alpha:]+\.com ]]; then

    echo "an email has been found!"

fi
#+end_src

The above compares $string to a regex (albiet probably a poor one) using character classes for matching an email address.

Also when using regular expressions this way allows you to group regex expressions as 'sub expressions' using 
parenthesis as the grouping mechanism. i.e. -

#+begin_src shell
if [[ $string =~ (pattern1)literal_string(pattern2) ]]
#+end_src

bash uses the results of those 'sub-expressions' to populate a bash built array variable called ${BASH_REMATCH}.  Each index with-in that array is assigned like so:

[0] - is the index whose value is the entire string matched by the entire regex expression
[1]..[N] - each subsequent index is the value of each sub-expression.  i.e. [1] is the first sub-expression, [2] is the second sub-expression, etc, etc.

i.e. - 

#+begin_src shell
if [[ $string =~ (pattern1)literal_string(pattern2) ]]; then 

    echo "The entire regex is ${BASH_REMATCH[0] and each sub-expression is ${BASH_REMATCH[1]} and ${BASH_REMATCH[2]}"

fi
#+end_src

Lastly is the topics of 'anchors' in regex.  The ^ character allows you to specify a regex to be matched at the beginning of a line and the $ symbol specifies the expression to be matched at the ending
of a line.  i.e. 

#+begin_src shell
string='jessieasdfasdfasdfjessie'

if [[ "$string" =~ (^.+literal*)|(.+literal*$) ]]
#+end_src

** _Review: Testing With Regular Expressions_

The below snippet applies using character classes with-in regex sub-expressions to match a string and then uses BASH_REMATCH builtin variables in psuedo bash statements:

#+begin_src shell
string='jessie.saenz@domain.com'

if [[ "$string" =~ ([[:alpha:][:digit:]]*)\.*([[:alpha:][:digit:]]*)@([[:alpha:][:digit:]]*).com ]]; then

    echo -e \\n "The email address ${BASH_REMATCH[0]} consists of ${BASH_REMATCH[1]}. as the first part, ${BASH_REMATCH[2]} as the second part and ${BASH_REMATCH[3]}.com as the domain"

fi
#+end_src


* _Testing If Running In A Terminal_

Like the -f or -nt file operator, there exists an operator that allows you to test if file descriptor is connected to a terminal and returns true if it is. i.e. - 

#+begin_src shell
if [ -t 1 ]; then

    echo "true"

fi
#+end_src


* _Piping Output To A Loop VS Redirecting Output To A Loop_

So you've learned from "Learning The Bash Shell" book how to redirect a file to a while loop and operate on the contents of that file.  You also (but not as vividly) remember learning how to do the same
by 'piping' the output of a file to a while loop and operating on it's contents.  However it's important to remember that any process operating on input from a pipe, operates on that data with-
in a subshell!  So in this instance any data that is created or changed with-in that loop who's input was 'piped' to it will not propogate upto the parent shell.  i.e.

#+begin_src shell
cat file | while read line; do array+=("$line"); done    #The $line variable won't exist once the loop finishes
#+end_src 

#+begin_src shell
while read line; do array+=("$line"); done < file        #The $line variable WILL still exist once the loop finishes
#+end_src

There is a benefit to piping output to a loop vs other means and it has to do with the sub-shell.  Remember how co-routines are implemented by running two different processes with-in subshell?  Well you
can use that attribute to your advantage if the output from one command will take a long time to process before it's ready for the receiving command, the pipe operator allows you to run command 1 and pipe
it's output in parallel to the receiving command and operate on it's data as it's coming in:

#+begin_src shell
for number in $(seq 1.0 .01 10000000000000000000000}; do    #You would see a blank screen for  a long while before the shell starts echoing the number in sequence

    echo $number

    sleep 1s

done
#+end_src

Now if you were to 'pipe' the info to a loop....

#+begin_src shell
seq 1.0 .01 10000000000000000000000000000000000000.1

| while read number; do                                 #You would see instantly the first echo of the number in the sequence because the while loop is processing data as the echo command is giving it

    echo $number

    sleep 1s

done
#+end_src


* _Looping With A Counter_

So.....with remembering the C style 'for loop' construct:

#+begin_src shell
for (i=0; $i < 10; i++); do

    echo $i

done
#+end_src

What I learned in this book is that you can extend an expression to include multiple operations.  i.e. - if you want to initialize multiple variables in that first line or set multiple conditions in the
second part.  i.e. - 

#+begin_src shell
for (i=0, j=5; $i < 10, $j >= 5; i++, j++); do

    echo $i and $j

done
#+end_src


* _Branching Many Ways_

So we remember case statements and how they allow you to do many efficient and neat if/then conditional operations in one easy to read structure.  If a match is found then the statements for that match
are executed.  Each statement has to end in ;; and after those statements execute the case statement is exited.

Well, there exist two additional operators that you can use to end statements with if you wanted  continuing looking for a match in the case statements and execute those commands associated with those
matches.  They are as follows:

;;& - This will allow you to examine the next pattern and if a match is found those statements will execute.

;& - This executes the statement associated with the next pattern regardless if a match is found with that pattern or not (personally I don't know why you would want that or not but...)


* _Best Practices For Error Messages_

If you've set a function or script to exit using the exit builtin and you want to echo an error, it's always best practice to redirect that output to STDERR.


* _Changing The Prompt On Simple Menu's_

PS1 - Displays the prompt in your shell/terminal

PS2 - Displays a prompt on a continued command line (the default is '>' and you see it when escaping a return key and are on a new-line on the terminal)

PS3 - Displays a prompt when using the select construct

PS4 - Displays a prompt before each line of output when a script is being run with xtrace on

